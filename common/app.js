/*****************************************************************************/
/* App: The Global Application Namespace */
/*****************************************************************************/
App = {};

SimpleSchema.messages({
  required: "[label] es un campo obligatorio",
  minString: "[label] debe tener al menos [min] caracteres",
  maxString: "[label] no debe tener más de [max] caracteres",
  minNumber: "[label] debe ser menor a [min]",
  maxNumber: "[label] no debe ser mayor a [max]",
  minDate: "[label] debe  ser [min] o anterior",
  maxDate: "[label] no debe ser posterior a [max]",
  minCount: "You must specify at least [minCount] values",
  maxCount: "You cannot specify more than [maxCount] values",
  noDecimal: "[label] debe ser un número entero",
  notAllowed: "[value] es un valor no permitido",
  expectedString: "[label] debe ser texto",
  expectedNumber: "[label] debe ser un número",
  expectedBoolean: "[label] debe ser un valor booleano",
  expectedArray: "[label] debe ser un array",
  expectedObject: "[label] debe ser un objeto",
  expectedConstructor: "[label] debe ser [type]",
  notUnique: "[label] ya existe. Debe ser único.",
  regEx: [
    {msg: "[label] failed regular expression validation"},
    {exp: SimpleSchema.RegEx.Email, msg: "[label] debe ser una dirección de email válida"},
    {exp: SimpleSchema.RegEx.WeakEmail, msg: "[label] must be a valid e-mail address"},
    {exp: SimpleSchema.RegEx.Domain, msg: "[label] must be a valid domain"},
    {exp: SimpleSchema.RegEx.WeakDomain, msg: "[label] must be a valid domain"},
    {exp: SimpleSchema.RegEx.IP, msg: "[label] must be a valid IPv4 or IPv6 address"},
    {exp: SimpleSchema.RegEx.IPv4, msg: "[label] must be a valid IPv4 address"},
    {exp: SimpleSchema.RegEx.IPv6, msg: "[label] must be a valid IPv6 address"},
    {exp: SimpleSchema.RegEx.Url, msg: "[label] must be a valid URL"},
    {exp: SimpleSchema.RegEx.Id, msg: "[label] must be a valid alphanumeric ID"}
  ],
  keyNotInSchema: "[label] is not allowed by the schema"
});


/*======================================================
=            Slingshots upload restrictions            =
======================================================*/

Slingshot.fileRestrictions("photoUpload", {
  allowedFileTypes: ["image/png", "image/jpeg", "image/gif"],
  maxSize: 0.5 * 1024 * 1024 // 0.5 MB (use null for unlimited).
});


