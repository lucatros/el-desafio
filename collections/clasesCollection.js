Clases = new Mongo.Collection('clases');

Clases.attachSchema(new SimpleSchema({
  fecha: {
    type: Date,
    label: "Fecha",
    autoform: {
      afFieldInput: {
        type: "bootstrap-datetimepicker",
        outMode: 'utcDate',
        dateTimePickerOptions: {
          format: 'L',
          locale: 'es',
          minDate: moment().startOf("year"),
          maxDate: moment().endOf("year")
        }
      }
    }
  },
  tallerId: {
    type: String,
    label: "Taller"
  },
  participantes: {
    type: Array,
    label: "Participante",
    optional: true
  },
  "participantes.$": {
    type: Object,
    optional: true
  },
  "participantes.$.id": {
    type: String,
    label: "Participante ID",
    optional: true
  },
  // "participantes.$.asistencia": {
  // 	type: String,
  // 	label: "Asistencia",
  // 	optional: true,
  // 	allowedValues: ["P", "A", "T", "J"],
  // 	autoform: {
  // 		type: "selectize",
  // 		afFieldInput: {
  // 		        isReactiveOptions: true,
  // 		        selectizeOptions: {
  // 		        	sortField: [] // Without this the order of the options is not correct in Chrome.
  // 		        }
  // 		      },
  // 		options: function () {
  // 			return [
  // 				{
  // 					optgroup: "Elegir asistencia",
  // 					options: [
  // 						{label: "Presente", value: "P"},
  // 						{label: "Ausente", value: "A"},
  // 						{label: "Tardanza", value: "T"},
  // 						{label: "Justificada", value: "J"}
  // 					]
  // 				}

  // 			];
  // 		}
  // 	}
  // },
  "participantes.$.asistencia": {
    type: String,
    label: "Asistencia",
    optional: true,
    allowedValues: ["P", "A", "T", "J"],
    autoform: {
      class: 'botonesAsistencia',
      options: [{
        label: "P",
        value: "P"
      }, {
        label: "A",
        value: "A"
      }, {
        label: "T",
        value: "T"
      }, {
        label: "J",
        value: "J"
      }]
    }
  },
  creadoFecha: {
    type: Date,
    label: "Creado fecha",
    autoValue: function() {
      if (this.isInsert) {
        return new Date;
      }
    }
  },
  creadoPor: {
    type: String,
    label: "Creado por",
    optional: true,
    autoValue: function() {
      if (this.isInsert) {
        return this.userId;
      }
    }
  },
  modificadoFecha: {
    type: Date,
    label: "Modificado fecha",
    optional: true,
    autoValue: function() {
      if (this.isUpdate) {
        return new Date;
      }
    }

  },
  modificadoPor: {
    type: String,
    label: "Modificado por",
    optional: true,
    autoValue: function() {
      if (this.isUpdate) {
        return this.userId;
      }
    }
  }
}));

SimpleSchema.debug = true;
