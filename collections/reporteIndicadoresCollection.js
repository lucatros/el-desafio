ReportesIndicadores = new Mongo.Collection('reportesIndicadores');

ReportesIndicadores.attachSchema(new SimpleSchema({
	fecha: {
		type: Date,
		label: "Fecha",
		autoform: {
			afFieldInput: {
				type: "bootstrap-datetimepicker",
				outMode: 'utcDate',
				dateTimePickerOptions: {
					format: 'MMMM (YYYY)',
					locale: 'es'
				}
			}
		}
	},

	talleres: {
		type: Array,
		label: "Talleres"
	},
	"talleres.$": {
		type: Object
	},
	"talleres.$._id": {
		type: String,
		label: "ID del taller",
	},
	"talleres.$.completado": {
		type: Boolean,
		label: "Completado",
	},
	
	"talleres.$.creadoFecha": {
		type: Date,
		label: "Creado fecha",
		optional: true,
		autoValue: function () {
			if (this.isUpdate) {
				return new Date;
			}
		}

	},
	"talleres.$.creadoPor": {
		type: String,
		label: "Creado por",
		optional: true,
		autoValue: function () {
			if (this.isUpdate) {
				return this.userId;
			}
		}
	}

})); 
