Participantes = new Mongo.Collection('participantes');

Participantes.attachSchema(new SimpleSchema({
  nombre: {
    type: String,
    label: "Nombre",
    max: 200,
    autoform: {
      placeholder: 'schemaLabel',
    }
  },
  apellido: {
    type: String,
    label: "Apellido",
    max: 200,
    autoform: {
      placeholder: 'schemaLabel',
    }
  },
  estrellas: {
    type: Number,
    label: "Estrellas",
    min: 0,
    max: 3,
    defaultValue: 3,
    optional: true,
  },
  activo: {
    type: Boolean,
    label: "Activo/Inactivo",
    defaultValue: true
  },
  foto: {
    type: String,
    label: "Foto",
    optional: true,
  },
  fechaNacimiento: {
    type: Date,
    label: "Fecha de nacimiento",
    autoform: {
      placeholder: 'schemaLabel',
      afFieldInput: {
        type: "bootstrap-datetimepicker",
        outMode: 'utcDate',
        dateTimePickerOptions: {
          format: 'L',
          locale: 'es'
        }
      }
    }
  },
  sexo: {
    type: String,
    label: "Sexo",
    allowedValues: ["mujer", "varón"],
    autoform: {
      type: "selectize",
      afFieldInput: {
        class: "XXXXXX",
        isReactiveOptions: true,
        selectizeOptions: {
          sortField: [] // Without this the order of the options is not correct in Chrome.
        }
      },
      options: function() {
        return [{
            optgroup: "Elegir sexo",
            options: [{
              label: "mujer",
              value: "mujer"
            }, {
              label: "varón",
              value: "varón"
            }]
        }];
      },
      placeholder: 'schemaLabel',
    }
  },
  dni: {
    type: String,
    label: "DNI",
    unique: true,
    autoform: {
      placeholder: 'Documento Nacional de Identidad',
    }
  },
  direccion: {
    type: String,
    label: "Dirección",
    optional: true,
    autoform: {
      placeholder: 'schemaLabel',
    }
  },
  ciudad: {
    type: String,
    label: "Ciudad",
    optional: true,
    autoform: {
      placeholder: 'schemaLabel',
    }
  },
  telefono: {
    type: String,
    label: "Teléfono",
    optional: true,
    autoform: {
      placeholder: 'schemaLabel',
    }
  },
  email: {
    type: String,
    label: "Email",
    unique: true,
    regEx: SimpleSchema.RegEx.Email,
    optional: true,
    autoform: {
      placeholder: 'schemaLabel',
    }
  },
  escuela: {
    type: String,
    label: "Escuela",
    optional: true,
    autoform: {
      placeholder: 'schemaLabel',
    }
  },
  gradoAno: {
    type: String,
    label: "Grado/Año",
    optional: true,
    allowedValues: ["1º grado", "2º grado", "3º grado", "4º grado", "5º grado", "6º grado", "7º grado", "1º año", "2º año", "3º año", "4º año", "5º año", "No escolarizado"],
    autoform: {
      placeholder: 'schemaLabel',
      type: "selectize",
      afFieldInput: {
        selectizeOptions: {
          sortField: [] // Without this the order of the options is not correct in Chrome.
        }
      },
      options: function() {
        return [{
            optgroup: "Grado",
            options: [{
              label: "1º grado",
              value: "1º grado"
            }, {
              label: "2º grado",
              value: "2º grado"
            }, {
              label: "3º grado",
              value: "3º grado"
            }, {
              label: "4º grado",
              value: "4º grado"
            }, {
              label: "5º grado",
              value: "5º grado"
            }, {
              label: "6º grado",
              value: "6º grado"
            }, {
              label: "7º grado",
              value: "7º grado"
            }]
          }, {
            optgroup: "Año",
            options: [{
              label: "1º año",
              value: "1º año"
            }, {
              label: "2º año",
              value: "2º año"
            }, {
              label: "3º año",
              value: "3º año"
            }, {
              label: "4º año",
              value: "4º año"
            }, {
              label: "5º año",
              value: "5º año"
            }, {
              label: "No escolarizado",
              value: "No escolarizado"
            }]
          }

        ];
      }
    }

  },
  obraSocial: {
    type: String,
    label: "Obra Social",
    optional: true,
    autoform: {
      placeholder: 'schemaLabel',
    }
  },
  srcFoto: {
    type: String,
    label: "Foto",
    optional: true
  },
  biografia: {
    type: String,
    label: "Biografía",
    optional: true,
    autoform: {
      afFieldInput: {
        type: 'summernote',
        class: 'editor', // optional
        // summernote options goes here
        lang: 'es-ES',
        height: 300,
      }
    }

  },
  docAptoMedico: {
    type: Boolean,
    label: "Apto Médico",
    optional: true
  },
  fechaAptoMedico: {
    type: Date,
    label: "Fecha apto médico",
    optional: true,
    autoform: {
      placeholder: 'schemaLabel',
      'label-class': 'col-sm-3',
      'input-col-class': 'col-sm-4',
      'template': 'bootstrap3-horizontal',
      afFieldInput: {
        type: "bootstrap-datetimepicker",
        outMode: 'utcDate',
        dateTimePickerOptions: {
          format: 'L',
          locale: 'es'
        }
      }
    }
  },
  docDni: {
    type: Boolean,
    label: "DNI",
    optional: true
  },
  docPartida: {
    type: Boolean,
    label: "Partida de nacimiento",
    optional: true
  },
  docCarnetVacuna: {
    type: Boolean,
    label: "Carné de vacunación",
    optional: true
  },
  compromisoPadres: {
    type: [String],
    label: "Los padres o tutores se comprometen a:",
    allowedValues: ["Voluntariado", "Donación Mensual"],
    optional: true,
    autoform: {
      placeholder: 'schemaLabel',
      'label-class': 'col-sm-4',
      'input-col-class': 'col-sm-4',
      'template': 'bootstrap3-horizontal',
      type: "selectize",
      afFieldInput: {
        multiple: true,
        selectizeOptions: {
          sortField: [] // Without this the order of the options is not correct in Chrome.
        }
      },
      options: function() {
        return [{
            optgroup: "Elegir una opción",
            options: [{
              label: "Voluntariado",
              value: "Voluntariado"
            }, {
              label: "Donación Mensual",
              value: "Donación Mensual"
            }]
          }

        ];
      }
    }
  },
  diario: {
    type: Array,
    label: "Diario",
    optional: true
  },
  "diario.$": {
    type: Object
  },
  "diario.$._id": {
    type: String,
    label: "ID del diario",
    autoValue: function() {
      return Random.id();
    }

  },
  "diario.$.fecha": {
    type: Date,
    label: "Fecha y hora",
    autoform: {
      afFieldInput: {
        type: "bootstrap-datetimepicker",
        outMode: 'utcDateTime',
        dateTimePickerOptions: {
          // format: 'LLL',
          locale: 'es'
        }
      }
    }
  },
  "diario.$.titulo": {
    type: String,
    label: "Título"
  },
  "diario.$.descripcion": {
    type: String,
    label: "Descripción",
  },

  familiares: {
    type: Array,
    label: "Familiares",
    optional: true
  },
  "familiares.$": {
    type: Object
  },
  "familiares.$.id": {
    type: String,
    label: "Familiar Id",
    optional: true
  },
  "familiares.$.relacion": {
    type: String,
    label: "Relación",
    optional: true
  },
  "familiares.$.emergencia": {
    type: Boolean,
    label: "Contacto de Emergencia",
    optional: true
  },

  datosMedicos: {
    type: Object,
    label: "Datos Médicos",
    optional: true
  },

  "datosMedicos.deporte": {
    type: String,
    label: "Actividad física o deporte que practica",
    optional: true,
    autoform: {
      placeholder: "schemaLabel"
    }
  },
  "datosMedicos.deporteAntiguedad": {
    type: String,
    label: "Antigüedad en esta actividad",
    optional: true,
    autoform: {
      placeholder: "schemaLabel"
    }
  },
  "datosMedicos.grupoSang": {
    type: String,
    label: "Grupo sanguíneo y RH",
    optional: true,
    allowedValues: ["A+", "A-", "B+", "B-", "AB+", "AB-", "0+", "0-"],
    autoform: {
      type: "selectize",
      placeholder: "schemaLabel",
      afFieldInput: {
        isReactiveOptions: true,
        selectizeOptions: {
          sortField: [] // Without this the order of the options is not correct in Chrome.
        }
      },
      options: function() {
        return [{
          optgroup: "Elegir grupo",
          options: [{
            label: "A+",
            value: "A+"
          }, {
            label: "A-",
            value: "A-"
          }, {
            label: "B+",
            value: "B+"
          }, {
            label: "B-",
            value: "B-"
          }, {
            label: "AB+",
            value: "AB+"
          }, {
            label: "AB-",
            value: "AB-"
          }, {
            label: "0+",
            value: "0+"
          }, {
            label: "0-",
            value: "0-"
          }]
        }];
      }
    }

  },
  "datosMedicos.embarazoNormal": {
    type: Boolean,
    label: "Embarazo Normal",
    optional: true
  },
  "datosMedicos.parto": {
    type: String,
    label: "Parto",
    optional: true,
    allowedValues: ["Normal", "Cesárea"],
    autoform: {
      type: "selectize",
      afFieldInput: {
        isReactiveOptions: true,
        placeholder: "schemaLabel",
        selectizeOptions: {
          sortField: [] // Without this the order of the options is not correct in Chrome.
        }
      },
      options: function() {
        return [{
          optgroup: "Elegir una opción",
          options: [{
            label: "Normal",
            value: "Normal"
          }, {
            label: "Cesárea",
            value: "Cesárea"
          }]
        }];
      }
    }
  },
  "datosMedicos.aTermino": {
    type: Boolean,
    label: "Nacido a término (9 meses)",
    optional: true
  },
  "datosMedicos.bcg": {
    type: Boolean,
    label: "BCG",
    optional: true
  },
  "datosMedicos.sabin": {
    type: Boolean,
    label: "Sabin",
    optional: true
  },
  "datosMedicos.triple": {
    type: Boolean,
    label: "Triple (DPT)",
    optional: true
  },
  "datosMedicos.doble": {
    type: Boolean,
    label: "Doble (DT)",
    optional: true
  },
  "datosMedicos.antitetanica": {
    type: Boolean,
    label: "Antitetánica",
    optional: true
  },
  "datosMedicos.antisarampionosa": {
    type: Boolean,
    label: "Antisarampionosa",
    optional: true
  },
  "datosMedicos.tripleViral": {
    type: Boolean,
    label: "Triple viral",
    optional: true
  },
  "datosMedicos.varicelaVacuna": {
    type: Boolean,
    label: "Varicela",
    optional: true
  },
  "datosMedicos.hib": {
    type: Boolean,
    label: "HIB (Haemophilus influenza tipo B)",
    optional: true
  },
  "datosMedicos.antiHepatitisA": {
    type: Boolean,
    label: "Anti-hepatitis A",
    optional: true
  },
  "datosMedicos.antiHepatitisB": {
    type: Boolean,
    label: "Anti-hepatitis B",
    optional: true
  },
  "datosMedicos.otrasVacunas": {
    type: String,
    label: "Otras (anotar nombre y cantidad de dosis)",
    optional: true,
    autoform: {
      placeholder: "schemaLabel"
    }
  },
  "datosMedicos.vacunaNoCompletadas": {
    type: String,
    label: "Anotar si alguna vacuna no se completó",
    optional: true
  },
  "datosMedicos.sarampion": {
    type: Boolean,
    label: "Sarampión",
    optional: true
  },
  "datosMedicos.rubeola": {
    type: Boolean,
    label: "Rubéola",
    optional: true
  },
  "datosMedicos.varicela": {
    type: Boolean,
    label: "Varicela",
    optional: true
  },
  "datosMedicos.paperas": {
    type: Boolean,
    label: "Paperas",
    optional: true
  },
  "datosMedicos.escarlatina": {
    type: Boolean,
    label: "Escarlatina",
    optional: true
  },
  "datosMedicos.hepatitis": {
    type: Boolean,
    label: "Hepatitis",
    optional: true
  },
  "datosMedicos.fiebreReumatica": {
    type: Boolean,
    label: "Fiebre reumática",
    optional: true
  },
  "datosMedicos.glomerulonefritis": {
    type: Boolean,
    label: "Glomerulonefritis",
    optional: true
  },
  "datosMedicos.otrasEnfermedadesInfancia": {
    type: String,
    label: "Otras, especificar",
    optional: true,
    autoform: {
      placeholder: "schemaLabel"
    }
  },
  "datosMedicos.diabetes": {
    type: Boolean,
    label: "Diabetes",
    optional: true
  },
  "datosMedicos.asma": {
    type: Boolean,
    label: "Asma",
    optional: true
  },
  "datosMedicos.enfermedadRenales": {
    type: Boolean,
    label: "Enfermedades renales",
    optional: true
  },
  "datosMedicos.enfermedadSangre": {
    type: Boolean,
    label: "Enfermedades de la sangre",
    optional: true
  },
  "datosMedicos.obesidad": {
    type: Boolean,
    label: "Obesidad",
    optional: true
  },
  "datosMedicos.cardioCongenita": {
    type: Boolean,
    label: "Cardiopatías congénitas",
    optional: true
  },
  "datosMedicos.hipertension": {
    type: Boolean,
    label: "Hipertensión arterial",
    optional: true
  },
  "datosMedicos.soplos": {
    type: Boolean,
    label: "Soplos",
    optional: true
  },
  "datosMedicos.arritmias": {
    type: Boolean,
    label: "Arritmias",
    optional: true
  },
  "datosMedicos.sinusitis": {
    type: Boolean,
    label: "Sinusitis",
    optional: true
  },
  "datosMedicos.otitis": {
    type: Boolean,
    label: "Otitis a repetición",
    optional: true
  },
  "datosMedicos.bronquitis": {
    type: Boolean,
    label: "Bronquitis a repetición",
    optional: true
  },
  "datosMedicos.neumonia": {
    type: Boolean,
    label: "Neumonía",
    optional: true
  },
  "datosMedicos.otrasEnfermedadesRespiratorias": {
    type: String,
    label: "Otras, especificar",
    optional: true,
    autoform: {
      placeholder: "schemaLabel"
    }
  },
  "datosMedicos.desgarros": {
    type: Boolean,
    label: "Desgarros musculares",
    optional: true
  },
  "datosMedicos.esguinces": {
    type: Boolean,
    label: "Esguinces",
    optional: true
  },
  "datosMedicos.fracturas": {
    type: Boolean,
    label: "Fracturas",
    optional: true
  },
  "datosMedicos.fracturasInfo": {
    type: String,
    label: "Especificar tipo y localización de la fractura, tiempo de inactividad y si hubo o no rehabilitación",
    optional: true,
    autoform: {
      placeholder: "schemaLabel"
    }
  },
  "datosMedicos.mareos": {
    type: Boolean,
    label: "Mareos en reposo",
    optional: true
  },
  "datosMedicos.desmayos": {
    type: Boolean,
    label: "Desmayos en reposo",
    optional: true
  },
  "datosMedicos.convulsiones": {
    type: Boolean,
    label: "Convulsiones",
    optional: true
  },
  "datosMedicos.convulsionesEjercicio": {
    type: Boolean,
    label: "Convulsiones después de ejercicio",
    optional: true
  },
  "datosMedicos.traumatismoCraneo": {
    type: Boolean,
    label: "Traumatismo de cráneo con internación",
    optional: true
  },
  "datosMedicos.celiaca": {
    type: Boolean,
    label: "Enfermedad celíaca",
    optional: true
  },
  "datosMedicos.parasitos": {
    type: Boolean,
    label: "Parásitos",
    optional: true
  },
  "datosMedicos.otrasEnfermedadesDigestivas": {
    type: String,
    label: "Otras, especificar",
    optional: true,
    autoform: {
      placeholder: "schemaLabel"
    }
  },
  "datosMedicos.enfermedadesPiel": {
    type: String,
    label: "Especificar",
    optional: true,
    autoform: {
      placeholder: "schemaLabel"
    }
  },
  "datosMedicos.anteojos": {
    type: Boolean,
    label: "Usa anteojos",
    optional: true
  },
  "datosMedicos.lentesContacto": {
    type: Boolean,
    label: "Usa lentes de contacto",
    optional: true
  },
  "datosMedicos.alergias": {
    type: String,
    label: "Especificar (medicamentos, alimentos, picaduras de insectos)",
    optional: true,
    autoform: {
      placeholder: "schemaLabel"
    }
  },
  "datosMedicos.medicamentos": {
    type: Boolean,
    label: "Está tomando medicamentos",
    optional: true
  },
  "datosMedicos.medicamentosInfo": {
    type: String,
    label: "Especificar nombres",
    optional: true,
    autoform: {
      placeholder: "schemaLabel"
    }
  },
  "datosMedicos.internado": {
    type: Boolean,
    label: "Ha estado internado alguna vez",
    optional: true
  },
  "datosMedicos.internadoInfo": {
    type: String,
    label: "Especificar motivo",
    optional: true,
    autoform: {
      placeholder: "schemaLabel"
    }
  },
  "datosMedicos.puedeActividad": {
    type: Boolean,
    label: "¿Cree Ud. que su hijo puede realizar actividad física?",
    optional: true
  },
  "datosMedicos.observaciones": {
    type: String,
    label: "Observaciones",
    optional: true,
    autoform: {
      placeholder: "schemaLabel"
    }
  },


  creadoFecha: {
    type: Date,
    label: "Creado fecha",
    autoValue: function() {
      if (this.isInsert) {
        return new Date;
      }
    }
  },
  creadoPor: {
    type: String,
    label: "Creado por",
    optional: true,
    autoValue: function() {
      if (this.isInsert) {
        return this.userId;
      }
    }
  },
  modificadoFecha: {
    type: Date,
    label: "Modificado fecha",
    optional: true,
    autoValue: function() {
      if (this.isUpdate) {
        return new Date;
      }
    }
  },
  modificadoPor: {
    type: String,
    label: "Modificado por",
    optional: true,
    autoValue: function() {
      if (this.isUpdate) {
        return this.userId;
      }
    }
  }
}));
