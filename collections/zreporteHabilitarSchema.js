// Este schema está agregado en el archivo usersCollection.js porque si no daba error.


Schema.ReporteHabilitar = new SimpleSchema({
  tipo: {
    type: String,
    label: "Tipo de reporte",
    allowedValues: ["Por indicadores", "Por objetivos"],
    autoform: {
      type: "selectize",
      afFieldInput: {
        class: "",
        isReactiveOptions: true,
        selectizeOptions: {
          sortField: [] // Without this the order of the options is not correct in Chrome.
        }
      },
      options: function() {
        return [{
          optgroup: "Elegir opción",
          options: [{
            label: "Por indicadores",
            value: "Por indicadores"
          }, {
            label: "Por objetivos",
            value: "Por objetivos"
          }]
        }];
      }
    }
  },
  fecha: {
    type: Date,
    label: "Fecha",
    autoform: {
      afFieldInput: {
        type: "bootstrap-datetimepicker",
        outMode: 'utcDate',
        dateTimePickerOptions: {
          format: 'MMMM (YYYY)',
          locale: 'es'
        }
      }
    }
  }
});
