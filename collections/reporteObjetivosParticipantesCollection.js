ReportesObjetivosParticipantes = new Mongo.Collection('reportesObjetivosParticipantes');

ReportesObjetivosParticipantes.attachSchema(new SimpleSchema({
  reporteObjetivosId: {
    type: String,
    label: "Reporte Objetivos ID"
  },
  tallerId: {
    type: String,
    label: "ID del taller"
  },
  participanteId: {
    type: String,
    label: "ID del participante"
  },
  completado: {
    type: Boolean,
    optional: true,
    label: "Completado"
  },

  respuestas: {
    type: Array,
    label: "Respuestas",
    optional: true
  },
  "respuestas.$": {
    type: Object
  },
  "respuestas.$.preguntaId": {
    type: String,
    label: "Pregunta Id"
  },
  "respuestas.$.respuesta": {
    type: String,
    label: "Respuesta",
    allowedValues: ["Sí", "No"],
    autoform: {
      type: "selectize",
      afFieldInput: {
        class: "",
        isReactiveOptions: true,
        selectizeOptions: {
          sortField: [] // Without this the order of the options is not correct in Chrome.
        }
      },
      options: function() {
        return [{
          optgroup: "Elegir opción",
          options: [{
            label: "Sí",
            value: "Sí"
          }, {
            label: "No",
            value: "No"
          }]
        }];
      }
    }
  },
  creadoFecha: {
    type: Date,
    label: "Creado fecha",
    optional: true,
    autoValue: function() {
      if (this.isUpdate) {
        return new Date;
      }
    }
  },
  creadoPor: {
    type: String,
    label: "Creado por",
    optional: true,
    autoValue: function() {
      if (this.isUpdate) {
        return this.userId;
      }
    }
  },
}));
