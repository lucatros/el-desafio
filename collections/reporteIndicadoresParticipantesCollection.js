ReportesIndicadoresParticipantes = new Mongo.Collection('reportesIndicadoresParticipantes');

ReportesIndicadoresParticipantes.attachSchema(new SimpleSchema({
  reporteIndicadoresId: {
    type: String,
    label: "Reporte Indicadores ID"
  },
  tallerId: {
    type: String,
    label: "ID del taller"
  },
  participanteId: {
    type: String,
    label: "ID del participante"
  },
  completado: {
    type: Boolean,
    optional: true,
    label: "Completado"
  },
  creadoFecha: {
    type: Date,
    label: "Creado fecha",
    optional: true,
    autoValue: function() {
      if (this.isUpdate) {
        return new Date;
      }
    }
  },
  creadoPor: {
    type: String,
    label: "Creado por",
    optional: true,
    autoValue: function() {
      if (this.isUpdate) {
        return this.userId;
      }
    }
  },

  area1_1: {
    type: String,
    label: "¿Establece dialogos con sus compañeros?",
    allowedValues: ["Sí", "No", "A veces"],
    autoform: {
      type: "selectize",
      afFieldInput: {
        class: "",
        isReactiveOptions: true,
        selectizeOptions: {
          sortField: [] // Without this the order of the options is not correct in Chrome.
        }
      },
      options: function() {
        return [{
          optgroup: "Elegir opción",
          options: [{
            label: "Sí",
            value: "Sí"
          }, {
            label: "No",
            value: "No"
          }, {
            label: "A veces",
            value: "A veces"
          }]
        }];
      }
    }
  },
  area1_2: {
    type: String,
    label: "¿Entiende y ejecuta las consignas?",
    allowedValues: ["Sí", "No", "A veces"],
    autoform: {
      type: "selectize",
      afFieldInput: {
        class: "",
        isReactiveOptions: true,
        selectizeOptions: {
          sortField: [] // Without this the order of the options is not correct in Chrome.
        }
      },
      options: function() {
        return [{
          optgroup: "Elegir opción",
          options: [{
            label: "Sí",
            value: "Sí"
          }, {
            label: "No",
            value: "No"
          }, {
            label: "A veces",
            value: "A veces"
          }]
        }];
      }
    }
  },
  area1_3: {
    type: String,
    label: "¿Utiliza su cuerpo como forma de expresión?",
    allowedValues: ["Sí", "No", "A veces"],
    autoform: {
      type: "selectize",
      afFieldInput: {
        class: "",
        isReactiveOptions: true,
        selectizeOptions: {
          sortField: [] // Without this the order of the options is not correct in Chrome.
        }
      },
      options: function() {
        return [{
          optgroup: "Elegir opción",
          options: [{
            label: "Sí",
            value: "Sí"
          }, {
            label: "No",
            value: "No"
          }, {
            label: "A veces",
            value: "A veces"
          }]
        }];
      }
    }
  },
  area1_4: {
    type: String,
    label: "¿Utiliza la violencia como forma de expresión?",
    allowedValues: ["Sí", "No", "A veces"],
    autoform: {
      type: "selectize",
      afFieldInput: {
        class: "",
        isReactiveOptions: true,
        selectizeOptions: {
          sortField: [] // Without this the order of the options is not correct in Chrome.
        }
      },
      options: function() {
        return [{
          optgroup: "Elegir opción",
          options: [{
            label: "Sí",
            value: "Sí"
          }, {
            label: "No",
            value: "No"
          }, {
            label: "A veces",
            value: "A veces"
          }]
        }];
      }
    }
  },
  area1_5: {
    type: String,
    label: "¿Se relaciona de manera adecuada con sus pares?",
    allowedValues: ["Sí", "No", "A veces"],
    autoform: {
      type: "selectize",
      afFieldInput: {
        class: "",
        isReactiveOptions: true,
        selectizeOptions: {
          sortField: [] // Without this the order of the options is not correct in Chrome.
        }
      },
      options: function() {
        return [{
          optgroup: "Elegir opción",
          options: [{
            label: "Sí",
            value: "Sí"
          }, {
            label: "No",
            value: "No"
          }, {
            label: "A veces",
            value: "A veces"
          }]
        }];
      }
    }
  },
  area1_6: {
    type: String,
    label: "¿Se relaciona de manera adecuada con los docentes?",
    allowedValues: ["Sí", "No", "A veces"],
    autoform: {
      type: "selectize",
      afFieldInput: {
        class: "",
        isReactiveOptions: true,
        selectizeOptions: {
          sortField: [] // Without this the order of the options is not correct in Chrome.
        }
      },
      options: function() {
        return [{
          optgroup: "Elegir opción",
          options: [{
            label: "Sí",
            value: "Sí"
          }, {
            label: "No",
            value: "No"
          }, {
            label: "A veces",
            value: "A veces"
          }]
        }];
      }
    }
  },
  area1_7: {
    type: String,
    label: "¿Establece relaciones de confianza con su docente?",
    allowedValues: ["Sí", "No", "A veces"],
    autoform: {
      type: "selectize",
      afFieldInput: {
        class: "",
        isReactiveOptions: true,
        selectizeOptions: {
          sortField: [] // Without this the order of the options is not correct in Chrome.
        }
      },
      options: function() {
        return [{
          optgroup: "Elegir opción",
          options: [{
            label: "Sí",
            value: "Sí"
          }, {
            label: "No",
            value: "No"
          }, {
            label: "A veces",
            value: "A veces"
          }]
        }];
      }
    }
  },
  area1_comentario: {
    type: String,
    optional: true,
    label: "Observaciones para Area Comunicación + Respeto + Conflicto",
    autoform: {
      rows: 3
    }
  },


  area2_1: {
    type: String,
    label: "¿Muestra conductas de colaboración?",
    allowedValues: ["Sí", "No", "A veces"],
    autoform: {
      type: "selectize",
      afFieldInput: {
        class: "",
        isReactiveOptions: true,
        selectizeOptions: {
          sortField: [] // Without this the order of the options is not correct in Chrome.
        }
      },
      options: function() {
        return [{
          optgroup: "Elegir opción",
          options: [{
            label: "Sí",
            value: "Sí"
          }, {
            label: "No",
            value: "No"
          }, {
            label: "A veces",
            value: "A veces"
          }]
        }];
      }
    }
  },
  area2_2: {
    type: String,
    label: "¿Participa activamente de todas las actividades propuestas?",
    allowedValues: ["Sí", "No", "A veces"],
    autoform: {
      type: "selectize",
      afFieldInput: {
        class: "",
        isReactiveOptions: true,
        selectizeOptions: {
          sortField: [] // Without this the order of the options is not correct in Chrome.
        }
      },
      options: function() {
        return [{
          optgroup: "Elegir opción",
          options: [{
            label: "Sí",
            value: "Sí"
          }, {
            label: "No",
            value: "No"
          }, {
            label: "A veces",
            value: "A veces"
          }]
        }];
      }
    }
  },
  area2_3: {
    type: String,
    label: "¿Respeta las reglas establecidas por El Desafío (horarios; orden; etc.)?",
    allowedValues: ["Sí", "No", "A veces"],
    autoform: {
      type: "selectize",
      afFieldInput: {
        class: "",
        isReactiveOptions: true,
        selectizeOptions: {
          sortField: [] // Without this the order of the options is not correct in Chrome.
        }
      },
      options: function() {
        return [{
          optgroup: "Elegir opción",
          options: [{
            label: "Sí",
            value: "Sí"
          }, {
            label: "No",
            value: "No"
          }, {
            label: "A veces",
            value: "A veces"
          }]
        }];
      }
    }
  },
  area2_4: {
    type: String,
    label: "¿Respeta los tiempos propios y de los demás?",
    allowedValues: ["Sí", "No", "A veces"],
    autoform: {
      type: "selectize",
      afFieldInput: {
        class: "",
        isReactiveOptions: true,
        selectizeOptions: {
          sortField: [] // Without this the order of the options is not correct in Chrome.
        }
      },
      options: function() {
        return [{
          optgroup: "Elegir opción",
          options: [{
            label: "Sí",
            value: "Sí"
          }, {
            label: "No",
            value: "No"
          }, {
            label: "A veces",
            value: "A veces"
          }]
        }];
      }
    }
  },
  area2_5: {
    type: String,
    label: "¿Interrumpe durante las actividades?",
    allowedValues: ["Sí", "No", "A veces"],
    autoform: {
      type: "selectize",
      afFieldInput: {
        class: "",
        isReactiveOptions: true,
        selectizeOptions: {
          sortField: [] // Without this the order of the options is not correct in Chrome.
        }
      },
      options: function() {
        return [{
          optgroup: "Elegir opción",
          options: [{
            label: "Sí",
            value: "Sí"
          }, {
            label: "No",
            value: "No"
          }, {
            label: "A veces",
            value: "A veces"
          }]
        }];
      }
    }
  },
  area2_6: {
    type: String,
    label: "¿Evidencia actitudes de Responsabilidad?",
    allowedValues: ["Sí", "No", "A veces"],
    autoform: {
      type: "selectize",
      afFieldInput: {
        class: "",
        isReactiveOptions: true,
        selectizeOptions: {
          sortField: [] // Without this the order of the options is not correct in Chrome.
        }
      },
      options: function() {
        return [{
          optgroup: "Elegir opción",
          options: [{
            label: "Sí",
            value: "Sí"
          }, {
            label: "No",
            value: "No"
          }, {
            label: "A veces",
            value: "A veces"
          }]
        }];
      }
    }
  },
  area2_7: {
    type: String,
    label: "¿Muestra actitudes de líder?",
    allowedValues: ["Sí", "No", "A veces"],
    autoform: {
      type: "selectize",
      afFieldInput: {
        class: "",
        isReactiveOptions: true,
        selectizeOptions: {
          sortField: [] // Without this the order of the options is not correct in Chrome.
        }
      },
      options: function() {
        return [{
          optgroup: "Elegir opción",
          options: [{
            label: "Sí",
            value: "Sí"
          }, {
            label: "No",
            value: "No"
          }, {
            label: "A veces",
            value: "A veces"
          }]
        }];
      }
    }
  },
  area2_8: {
    type: String,
    label: "¿Mantiene una adecuada higiene personal?",
    allowedValues: ["Sí", "No", "A veces"],
    autoform: {
      type: "selectize",
      afFieldInput: {
        class: "",
        isReactiveOptions: true,
        selectizeOptions: {
          sortField: [] // Without this the order of the options is not correct in Chrome.
        }
      },
      options: function() {
        return [{
          optgroup: "Elegir opción",
          options: [{
            label: "Sí",
            value: "Sí"
          }, {
            label: "No",
            value: "No"
          }, {
            label: "A veces",
            value: "A veces"
          }]
        }];
      }
    }
  },
  area2_9: {
    type: String,
    label: "¿Se ocupa de su arreglo personal?",
    allowedValues: ["Sí", "No", "A veces"],
    autoform: {
      type: "selectize",
      afFieldInput: {
        class: "",
        isReactiveOptions: true,
        selectizeOptions: {
          sortField: [] // Without this the order of the options is not correct in Chrome.
        }
      },
      options: function() {
        return [{
          optgroup: "Elegir opción",
          options: [{
            label: "Sí",
            value: "Sí"
          }, {
            label: "No",
            value: "No"
          }, {
            label: "A veces",
            value: "A veces"
          }]
        }];
      }
    }
  },
  area2_10: {
    type: String,
    label: "¿Evidencia hábitos de orden y limpieza?",
    allowedValues: ["Sí", "No", "A veces"],
    autoform: {
      type: "selectize",
      afFieldInput: {
        class: "",
        isReactiveOptions: true,
        selectizeOptions: {
          sortField: [] // Without this the order of the options is not correct in Chrome.
        }
      },
      options: function() {
        return [{
          optgroup: "Elegir opción",
          options: [{
            label: "Sí",
            value: "Sí"
          }, {
            label: "No",
            value: "No"
          }, {
            label: "A veces",
            value: "A veces"
          }]
        }];
      }
    }
  },
  area2_11: {
    type: String,
    label: "¿Cuida las instalaciones?",
    allowedValues: ["Sí", "No", "A veces"],
    autoform: {
      type: "selectize",
      afFieldInput: {
        class: "",
        isReactiveOptions: true,
        selectizeOptions: {
          sortField: [] // Without this the order of the options is not correct in Chrome.
        }
      },
      options: function() {
        return [{
          optgroup: "Elegir opción",
          options: [{
            label: "Sí",
            value: "Sí"
          }, {
            label: "No",
            value: "No"
          }, {
            label: "A veces",
            value: "A veces"
          }]
        }];
      }
    }
  },
  area2_12: {
    type: String,
    label: "¿Cuida los materiales?",
    allowedValues: ["Sí", "No", "A veces"],
    autoform: {
      type: "selectize",
      afFieldInput: {
        class: "",
        isReactiveOptions: true,
        selectizeOptions: {
          sortField: [] // Without this the order of the options is not correct in Chrome.
        }
      },
      options: function() {
        return [{
          optgroup: "Elegir opción",
          options: [{
            label: "Sí",
            value: "Sí"
          }, {
            label: "No",
            value: "No"
          }, {
            label: "A veces",
            value: "A veces"
          }]
        }];
      }
    }
  },
  area2_comentario: {
    type: String,
    optional: true,
    label: "Observaciones para Area Trabajo en Equipo + Pertenencia + Adueñarse + Compromiso + Liderazgo",
    autoform: {
      rows: 3
    }
  },


  area3_1: {
    type: String,
    label: "¿Se muestra independiente dentro del taller?",
    allowedValues: ["Sí", "No", "A veces"],
    autoform: {
      type: "selectize",
      afFieldInput: {
        class: "",
        isReactiveOptions: true,
        selectizeOptions: {
          sortField: [] // Without this the order of the options is not correct in Chrome.
        }
      },
      options: function() {
        return [{
          optgroup: "Elegir opción",
          options: [{
            label: "Sí",
            value: "Sí"
          }, {
            label: "No",
            value: "No"
          }, {
            label: "A veces",
            value: "A veces"
          }]
        }];
      }
    }
  },
  area3_2: {
    type: String,
    label: "¿Puede tomar decisiones?",
    allowedValues: ["Sí", "No", "A veces"],
    autoform: {
      type: "selectize",
      afFieldInput: {
        class: "",
        isReactiveOptions: true,
        selectizeOptions: {
          sortField: [] // Without this the order of the options is not correct in Chrome.
        }
      },
      options: function() {
        return [{
          optgroup: "Elegir opción",
          options: [{
            label: "Sí",
            value: "Sí"
          }, {
            label: "No",
            value: "No"
          }, {
            label: "A veces",
            value: "A veces"
          }]
        }];
      }
    }
  },
  area3_3: {
    type: String,
    label: "¿Adopta un rol pasivo durante las actividades?",
    allowedValues: ["Sí", "No", "A veces"],
    autoform: {
      type: "selectize",
      afFieldInput: {
        class: "",
        isReactiveOptions: true,
        selectizeOptions: {
          sortField: [] // Without this the order of the options is not correct in Chrome.
        }
      },
      options: function() {
        return [{
          optgroup: "Elegir opción",
          options: [{
            label: "Sí",
            value: "Sí"
          }, {
            label: "No",
            value: "No"
          }, {
            label: "A veces",
            value: "A veces"
          }]
        }];
      }
    }
  },
  area3_4: {
    type: String,
    label: "¿Muestra conductas intolerantes?",
    allowedValues: ["Sí", "No", "A veces"],
    autoform: {
      type: "selectize",
      afFieldInput: {
        class: "",
        isReactiveOptions: true,
        selectizeOptions: {
          sortField: [] // Without this the order of the options is not correct in Chrome.
        }
      },
      options: function() {
        return [{
          optgroup: "Elegir opción",
          options: [{
            label: "Sí",
            value: "Sí"
          }, {
            label: "No",
            value: "No"
          }, {
            label: "A veces",
            value: "A veces"
          }]
        }];
      }
    }
  },
  area3_5: {
    type: String,
    label: "¿Se irrita con frecuencia?",
    allowedValues: ["Sí", "No", "A veces"],
    autoform: {
      type: "selectize",
      afFieldInput: {
        class: "",
        isReactiveOptions: true,
        selectizeOptions: {
          sortField: [] // Without this the order of the options is not correct in Chrome.
        }
      },
      options: function() {
        return [{
          optgroup: "Elegir opción",
          options: [{
            label: "Sí",
            value: "Sí"
          }, {
            label: "No",
            value: "No"
          }, {
            label: "A veces",
            value: "A veces"
          }]
        }];
      }
    }
  },
  area3_6: {
    type: String,
    label: "¿Es susceptible a las críticas?",
    allowedValues: ["Sí", "No", "A veces"],
    autoform: {
      type: "selectize",
      afFieldInput: {
        class: "",
        isReactiveOptions: true,
        selectizeOptions: {
          sortField: [] // Without this the order of the options is not correct in Chrome.
        }
      },
      options: function() {
        return [{
          optgroup: "Elegir opción",
          options: [{
            label: "Sí",
            value: "Sí"
          }, {
            label: "No",
            value: "No"
          }, {
            label: "A veces",
            value: "A veces"
          }]
        }];
      }
    }
  },
  area3_7: {
    type: String,
    label: "¿Puede recomenzar las actividades cuando se equivoca?",
    allowedValues: ["Sí", "No", "A veces"],
    autoform: {
      type: "selectize",
      afFieldInput: {
        class: "",
        isReactiveOptions: true,
        selectizeOptions: {
          sortField: [] // Without this the order of the options is not correct in Chrome.
        }
      },
      options: function() {
        return [{
          optgroup: "Elegir opción",
          options: [{
            label: "Sí",
            value: "Sí"
          }, {
            label: "No",
            value: "No"
          }, {
            label: "A veces",
            value: "A veces"
          }]
        }];
      }
    }
  },
  area3_8: {
    type: String,
    label: "¿Logra alcanzar los objetivos propuestos por el taller/actividad?",
    allowedValues: ["Sí", "No", "A veces"],
    autoform: {
      type: "selectize",
      afFieldInput: {
        class: "",
        isReactiveOptions: true,
        selectizeOptions: {
          sortField: [] // Without this the order of the options is not correct in Chrome.
        }
      },
      options: function() {
        return [{
          optgroup: "Elegir opción",
          options: [{
            label: "Sí",
            value: "Sí"
          }, {
            label: "No",
            value: "No"
          }, {
            label: "A veces",
            value: "A veces"
          }]
        }];
      }
    }
  },
  area3_9: {
    type: String,
    label: "¿Puede superar sus inhibiciones?",
    allowedValues: ["Sí", "No", "A veces"],
    autoform: {
      type: "selectize",
      afFieldInput: {
        class: "",
        isReactiveOptions: true,
        selectizeOptions: {
          sortField: [] // Without this the order of the options is not correct in Chrome.
        }
      },
      options: function() {
        return [{
          optgroup: "Elegir opción",
          options: [{
            label: "Sí",
            value: "Sí"
          }, {
            label: "No",
            value: "No"
          }, {
            label: "A veces",
            value: "A veces"
          }]
        }];
      }
    }
  },
  area3_comentario: {
    type: String,
    optional: true,
    label: "Observaciones para Area Autoestima + Confianza + Frustración + Identidad + Superación",
    autoform: {
      rows: 3
    }
  },
}));
