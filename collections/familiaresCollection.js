Familiares = new Mongo.Collection('familiares');

Familiares.attachSchema(new SimpleSchema({
	nombre: {
		type: String,
		label: "Nombre",
		max: 200,
		autoform: {
			placeholder: "schemaLabel"
		}
	},
	apellido: {
		type: String,
		label: "Apellido",
		max: 200,
		autoform: {
			placeholder: "schemaLabel"
		}
	},
	fechaNacimiento: {
		type: Date,
		label: "Fecha nac.",
		autoform: {
			placeholder: "Fecha de nacimiento",
			afFieldInput: {
				type: "bootstrap-datetimepicker",
				outMode: 'utcDate',
				dateTimePickerOptions: {
					format: 'L',
					locale: 'es'
				}
			}
		}
	},
	email: {
		type: String,
		label: "Email",
		regEx: SimpleSchema.RegEx.Email,
		optional: true,
		autoform: {
			placeholder: "schemaLabel"
		}

	},
	telefono1: {
		type: String,
		label: "Teléfono 1",
		optional: true,
		autoform: {
			placeholder: "schemaLabel"
		}
	},
	telefono2: {
		type: String,
		label: "Teléfono 2",
		optional: true,
		autoform: {
			placeholder: "schemaLabel"
		}
	},
	ocupacion: {
		type: String,
		label: "Ocupación",
		optional: true,
		autoform: {
			placeholder: "schemaLabel"
		}
	},
	estudios: {
		type: String,
		label: "Estudios",
		optional: true,
		autoform: {
			placeholder: "schemaLabel"
		}
	},
	creadoFecha: {
		type: Date,
		label: "Creado fecha",
		autoValue: function () {
			if (this.isInsert) {
				return new Date;
			}
		}
	},
	creadoPor: {
		type: String,
		label: "Creado por",
		optional: true,
		autoValue: function () {
			if (this.isInsert) {
				return this.userId;
			}
		}
	},
	modificadoFecha: {
		type: Date,
		label: "Modificado fecha",
		optional: true,
		autoValue: function () {
			if (this.isUpdate) {
				return new Date;
			}
		}

	},
	modificadoPor: {
		type: String,
		label: "Modificado por",
		optional: true,
		autoValue: function () {
			if (this.isUpdate) {
				return this.userId;
			}
		}
	}

})); 
