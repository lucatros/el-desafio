Talleres = new Mongo.Collection('talleres');

Talleres.attachSchema(new SimpleSchema({
  nombre: {
    type: String,
    label: "Nombre",
    max: 200,
  },
  ano: {
    type: Number,
    label: "Año del taller",
    autoValue: function() {
      var d = new Date();
      var n = d.getFullYear();
      return n;
    }
  },
  cupo: {
    type: Number,
    label: "Cupo",
    max: 200,
    min: 1
  },
  edadDesde: {
    type: Number,
    label: "Desde",
    min: 1,
    max: 110
  },
  edadHasta: {
    type: Number,
    label: "Hasta",
    min: 1,
    max: 110
  },
  edadAnoCalendario: {
    type: Boolean,
    label: "Calcular edad del taller basándose solamente en el año de nacimiento", // Esto es porque para algunos talleres se tienen en cuenta la edad al 31 de junio y en otros al 1 de enero.
    optional: true
  },

  sexo: {
    type: String,
    label: "Sexo",
    allowedValues: ["mujer", "varón", "mixto"],
    autoform: {
      type: "selectize",
      options: function() {
        return [{
          optgroup: "Elegir Sexo",
          options: [{
            label: "mujer",
            value: "mujer"
          }, {
            label: "varón",
            value: "varón"
          }, {
            label: "mixto",
            value: "mixto"
          }]
        }];
      }
    }
  },
  diaHora: {
    type: Array,
    label: "Día y hora",
    optional: true,
    minCount: 1
  },
  "diaHora.$": {
    type: Object
  },
  "diaHora.$.dia": {
    type: String,
    label: "Día",
    optional: true,
    allowedValues: ["lunes", "martes", "miercoles", "jueves", "viernes", "sabado", "domingo"],
    autoform: {
      type: "selectize",
      options: function() {
        return [{
          optgroup: "Elegir día",
          options: [{
            label: "Lunes",
            value: "lunes"
          }, {
            label: "Martes",
            value: "martes"
          }, {
            label: "Miércoles",
            value: "miercoles"
          }, {
            label: "Jueves",
            value: "jueves"
          }, {
            label: "Viernes",
            value: "viernes"
          }, {
            label: "Sábado",
            value: "sabado"
          }, {
            label: "Domingo",
            value: "domingo"
          }]
        }];
      },
    }
  },
  "diaHora.$.horaInicio": {
    type: Date,
    label: "Inicio",
    optional: true,
    autoform: {
      afFieldInput: {
        type: "bootstrap-datetimepicker",
        outMode: 'utcDateTime', // 'utcDate', 'utcDateTime'
        dateTimePickerOptions: {
          format: 'LT',
          locale: 'es',
          useCurrent: 'day'
        }
      }
    }
  },
  "diaHora.$.horaFin": {
    type: Date,
    label: "Fin",
    optional: true,
    autoform: {
      afFieldInput: {
        type: "bootstrap-datetimepicker",
        outMode: 'utcDateTime',
        dateTimePickerOptions: {
          format: 'LT',
          locale: 'es',
          useCurrent: 'day'
        }
      }
    }
  },
  objetivo: {
    type: String,
    label: "Objetivo",
    optional: true,
    autoform: {
      afFieldInput: {
        type: 'summernote',
        class: 'editor', // optional
        // summernote options goes here
        lang: 'es-ES',
        height: 300,
      }
    }
  },
  talleristas: {
    type: [String],
    label: "Tallerista(s)",
    optional: true,
    autoform: {
      type: "selectize",
      afFieldInput: {
        multiple: true,
        selectizeOptions: {
          sortField: [] // Without this the order of the options is not correct in Chrome.
        }
      },
      options: function() {
        return Users.find({
          roles: {
            $in: ['tallerista']
          }
        }).map(function(obj) {
          return {
            label: obj.profile.apellido + ', ' + obj.profile.nombre,
            value: obj._id
          };
        });
      }
    }

  },
  voluntarios: {
    type: [String],
    label: "Voluntario(s)",
    optional: true,
    autoform: {
      type: "selectize",
      afFieldInput: {
        multiple: true,
        selectizeOptions: {
          sortField: [] // Without this the order of the options is not correct in Chrome.
        }
      },
      options: function() {
        return Users.find({
          roles: {
            $in: ['voluntarioTallerista']
          }
        }).map(function(obj) {
          return {
            label: obj.profile.apellido + ', ' + obj.profile.nombre,
            value: obj._id
          };
        });
      }
    }

  },
  diario: {
    type: Array,
    label: "Diario",
    optional: true
  },
  "diario.$": {
    type: Object
  },
  "diario.$._id": {
    type: String,
    label: "ID del diario",
    autoValue: function() {
      return Random.id();
    }

  },
  "diario.$.fecha": {
    type: Date,
    label: "Fecha y hora",
    autoform: {
      afFieldInput: {
        type: "bootstrap-datetimepicker",
        outMode: 'utcDateTime',
        dateTimePickerOptions: {
          // format: 'LLL',
          locale: 'es'
        }
      }
    }
  },
  "diario.$.titulo": {
    type: String,
    label: "Título"
  },
  "diario.$.descripcion": {
    type: String,
    label: "Descripción",
  },
  indicadores: {
    type: Array,
    label: "Indicadores",
    optional: true,
  },
  "indicadores.$": {
    type: Object
  },
  "indicadores.$.indicador": {
    type: String,
    label: "Indicador"
  },
  // "indicadores.$.preguntas": {
  // 	type: Array,
  // 	label: "Preguntas"
  // },
  // "indicadores.$.preguntas.$": {
  // 	type: String,
  // 	label: "Pregunta"
  // },
  "indicadores.$.preguntas": {
    type: Array,
    label: "Preguntas"
  },
  "indicadores.$.preguntas.$": {
    type: Object
  },
  "indicadores.$.preguntas.$.pregunta": {
    type: String,
    label: "Pregunta"
  },
  "indicadores.$.preguntas.$._id": {
    type: String,
    label: "ID de la pregunta",
    autoValue: function() {
      return Random.id();
    }

  },


  participantesActivos: {
    type: Array,
    label: "Participantes activos",
    optional: true,
  },
  "participantesActivos.$": {
    type: String,
    optional: true
  },
  participantesInactivos: {
    type: Array,
    label: "Participantes inactivos",
    optional: true,
  },
  "participantesInactivos.$": {
    type: String,
    optional: true
  },
  participantesEspera: {
    type: Array,
    label: "Participantes en lista de espera",
    optional: true,
  },
  "participantesEspera.$": {
    type: String,
    optional: true
  },
  clases: {
    type: [String],
    label: "Clases",
    optional: true
  },

  creadoFecha: {
    type: Date,
    label: "Creado fecha",
    autoValue: function() {
      if (this.isInsert) {
        return new Date;
      }
    }
  },
  creadoPor: {
    type: String,
    label: "Creado por",
    optional: true,
    autoValue: function() {
      if (this.isInsert) {
        return this.userId;
      }
    }
  },
  modificadoFecha: {
    type: Date,
    label: "Modificado fecha",
    optional: true,
    autoValue: function() {
      if (this.isUpdate) {
        return new Date;
      }
    }

  },
  modificadoPor: {
    type: String,
    label: "Modificado por",
    optional: true,
    autoValue: function() {
      if (this.isUpdate) {
        return this.userId;
      }
    }
  }
}));

SimpleSchema.debug = true;
