Users = Meteor.users;

Schema = {};

Schema.UserProfile = new SimpleSchema({
  nombre: {
    type: String,
    label: "Nombre"
  },
  apellido: {
    type: String,
    label: "Apellido"
  },
  fechaNacimiento: {
    type: Date,
    label: "Fecha de nacimiento",
    optional: true,
    autoform: {
      afFieldInput: {
        type: "bootstrap-datetimepicker",
        outMode: 'utcDate',
        dateTimePickerOptions: {
          format: 'L',
          locale: 'es'
        }
      }
    }
  },
  foto: {
    type: String,
    label: "Foto",
    optional: true,
  },
  direccion: {
    type: String,
    label: "Dirección",
    optional: true
  },
  ciudad: {
    type: String,
    label: "Ciudad",
    optional: true
  },
  telefono1: {
    type: String,
    label: "Teléfono1",
    optional: true
  },
  telefono2: {
    type: String,
    label: "Teléfono2",
    optional: true
  },
  creadoFecha: {
    type: Date,
    label: "Creado fecha",
    autoValue: function() {
      if (this.isInsert) {
        return new Date;
      }
    }
  },
  creadoPor: {
    type: String,
    label: "Creado por",
    optional: true,
    autoValue: function() {
      if (this.isInsert) {
        return this.userId;
      }
    }
  },
  modificadoFecha: {
    type: Date,
    label: "Modificado fecha",
    optional: true,
    autoValue: function() {
      if (this.isUpdate) {
        return new Date;
      }
    }

  },
  modificadoPor: {
    type: String,
    label: "Modificado por",
    optional: true,
    autoValue: function() {
      if (this.isUpdate) {
        return this.userId;
      }
    }
  }

});

Schema.User = new SimpleSchema({
  username: {
    type: String,
    regEx: /^[a-z0-9A-Z_]{3,15}$/,
    optional: true
  },
  emails: {
    type: [Object],
    // this must be optional if you also use other login services like facebook,
    // but if you use only accounts-password, then it can be required
    // optional: true
  },
  "emails.$.address": {
    type: String,
    regEx: SimpleSchema.RegEx.Email,
    label: "Email",
    autoform: {
      placeholder: "Correo electrónico"
    }
  },
  "emails.$.verified": {
    type: Boolean,
    optional: true
  },
  createdAt: {
    type: Date,
    optional: true
  },
  profile: {
    type: Schema.UserProfile
  },
  services: {
    type: Object,
    optional: true,
    blackbox: true
  },
  // Add `roles` to your schema if you use the meteor-roles package.
  // Option 1: Object type
  // If you specify that type as Object, you must also specify the
  // `Roles.GLOBAL_GROUP` group whenever you add a user to a role.
  // Example:
  // Roles.addUsersToRoles(userId, ["admin"], Roles.GLOBAL_GROUP);
  // You can't mix and match adding with and without a group since
  // you will fail validation in some cases.
  // roles: {
  //     type: Object,
  //     optional: true,
  //     blackbox: true
  // },
  // Option 2: [String] type
  // If you are sure you will never need to use role groups, then
  // you can specify [String] as the type
  roles: {
    type: [String],
    label: "Rol",
    allowedValues: ["admin", "tallerista", "voluntarioTallerista"],
    autoform: {
      type: "selectize",
      afFieldInput: {
        multiple: true,
        selectizeOptions: {
          sortField: [] // Without this the order of the options is not correct in Chrome.
        }
      },
      options: function() {
        return [{
          optgroup: "Elegir el rol del usuario",
          options: [{
            label: "Administrador",
            value: "admin"
          }, {
            label: "Tallerista",
            value: "tallerista"
          }, {
            label: "Voluntario Tallerista",
            value: "voluntarioTallerista"
          }]
        }];
      }
    }
  }
});

Meteor.users.attachSchema(Schema.User);
