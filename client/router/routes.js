// Ensures all routes are protected by default.
FlowRouter.triggers.enter([AccountsTemplates.ensureSignedIn]);


// Home just redirects to participantes.home
FlowRouter.route('/', {
  name: 'home',
  action() {
    FlowRouter.go('participantes.home');
  }
});

// Not Found
FlowRouter.notFound = {
  action() {
    BlazeLayout.render('MasterLayout', {main: 'notFound'});
  }
};


/*============================================
=            Participantes Routes            =
============================================*/

FlowRouter.route('/participantes', {
  name: 'participantes.home',
  action() {
    BlazeLayout.render('MasterLayout', {main: 'participantesHome'});
  }
});

FlowRouter.route('/participante/form/:_id', {
  name: 'participante.form',
  action() {
    BlazeLayout.render('MasterLayout', {main: 'participanteForm'});
  }
});

/*=======================================
=            Accounts Routes            =
=======================================*/

AccountsTemplates.configureRoute('signIn', {
  name: 'signin',
  path: '/signin'
});

AccountsTemplates.configureRoute('forgotPwd');

AccountsTemplates.configureRoute('resetPwd', {
  name: 'resetPwd',
  path: '/reset-password'
});
