/* ****************************************************************************/
/* Client App Namespace  */
/* ****************************************************************************/
// _.extend(App, {});

// App.helpers = {};

// _.each(App.helpers, function(helper, key) {
//   Handlebars.registerHelper(key, helper);
// });


/* ====================================================
=            Inicializar algunas Sessions            =
==================================================== */

if (Meteor.isClient) {
  Meteor.startup(() => {
    let mesActual = moment.utc().month();
    Session.set('mesFiltro', mesActual);

    // Inicializar filtros de la tabla de participantes
    Session.set('verColumnaFoto', true);
    Session.set('verColumnaEdad', true);
    Session.set('verColumnaEscuela', false);
    Session.set('verColumnaGradoAno', false);
    Session.set('verColumnaSexo', false);
    Session.set('verColumnaDNI', false);
    Session.set('verColumnaEscolarizado', false);
    Session.set('verColumnaEstrellas', true);
    Session.set('verColumnaDocumentos', true);
    Session.set('verInactivosAsistencia', false);
  });
}


/* -----  End of Inicializar algunas Sessions  ------*/


/* ======================================
=            Global Helpers            =
======================================*/

Template.registerHelper('calcularEdad', (date) => {
  return moment().utc(date).diff(date, 'years');
});

Template.registerHelper('renderEstrellas', (estrellas) => {
  let html = "";
  let estrellasOff = 3 - estrellas;

  for (estrellas; estrellas > 0; estrellas--) {
    html = html + '<span class="fa fa-star estrella"></span>';
  }

  for (estrellasOff; estrellasOff > 0; estrellasOff--) {
    html = html + '<span class="fa fa-star estrella estrella-off"></span>';
  }

  return html;
});

// Format Time of the Taller
Template.registerHelper('formatTimeTaller', (date) => {
  return moment.utc(date).format("H:mm");
});

// Format Fecha
Template.registerHelper('formatFecha', (date) => {
  moment.locale('es');
  return moment.utc(date).format("LL");
});

// Format Fecha y hora
Template.registerHelper('formatFechaHora', (date) => {
  moment.locale('es');
  return moment.utc(date).format("DD/MM/YY H:mm") + ' hs.';
});

// Format Fecha y hora
Template.registerHelper('formatFechaHoraForSorting', (date) => {
  moment.locale('es');
  return moment.utc(date).format('X');
});

// Format Fecha y hora
Template.registerHelper('formatFechaReporte', (date) => {
  moment.locale('es');
  return moment.utc(date).format("MMMM (YYYY)");
});













// NOTE: Revisar lo de abajo



/* ========================================================
=            Global Helpers para los permisos            =
========================================================*/

Template.registerHelper('esAdminTalleristaVoluntarioDelTaller', function(user, tallerId) {
  var taller = Talleres.findOne(tallerId);
  if ((_.contains(user.roles, 'admin')) ||
    (_.contains(user.roles, 'tallerista') && _.contains(taller.talleristas, user._id)) ||
    (_.contains(user.roles, 'voluntarioTallerista') && _.contains(taller.voluntarios, user._id))) {
    return true;
  } else {
    return false;
  }
});

Template.registerHelper('esAdminTalleristaDelTaller', function(user, tallerId) {
  var taller = Talleres.findOne(tallerId);
  if ((_.contains(user.roles, 'admin')) ||
    (_.contains(user.roles, 'tallerista') && _.contains(taller.talleristas, user._id))) {
    return true;
  } else {
    return false;
  }
});

// Ver si es el tallereista o voluntario que creó la clase y solamente devolver true si no pasó mas de 1 semana o si es admin
Template.registerHelper('esOwnerClaseNoPasoSemana', function(user, claseId, tallerId) {
  var clase = Clases.findOne(claseId);
  var now = moment(now);
  var diasDesdeCreacion = now.diff(clase.creadoFecha, 'days');
  if (_.contains(user.roles, 'admin')) {
    return true;
  } else if (((_.contains(user.roles, 'tallerista') && user._id === clase.creadoPor) ||
      (_.contains(user.roles, 'voluntarioTallerista') && user._id === clase.creadoPor)) &&
    diasDesdeCreacion < 7) {
    return true;
  } else {
    return false;
  }
});

/* -----  End of Global Helpers para los permisos  ------*/


/* ===============================================================================
=            Formatos para la fecha de la columna de las asistencias            =
===============================================================================*/

Template.registerHelper('formatFechaClaseDiaTxt', function(date) {
  moment.locale('es');
  return moment.utc(date).format('ddd');
});

Template.registerHelper('formatFechaClaseDiaNum', function(date) {
  moment.locale('es');
  return moment.utc(date).format('DD');
});

Template.registerHelper('formatFechaClaseMes', function(date) {
  moment.locale('es');
  return moment.utc(date).format('MMM');
});

Template.registerHelper('formatFechaClaseAno', function(date) {
  moment.locale('es');
  return moment.utc(date).format('YYYY');
});


/* -----  End of Formatos para la fecha de la columna de las asistencias  ------*/

// Check mark para True or False
Template.registerHelper('checkmark', function(boolean) {
  if (boolean === true) {
    return '<span class="fui-check text-primary">';
  } else {
    return '</span><span class="fui-cross text-danger">';
  }
});

// Contar Arrays
Template.registerHelper('contarArray', function(array) {
  if (array !== null) {
    return array.length;
  } else {
    return '0';
  }
});

// Contar Collection
Template.registerHelper('contarCollection', function(collection) {
  if (collection) {
    return collection.count();
  } else {
    return '0';
  }
});

// Devuelve el nombre y apellido de un User
Template.registerHelper('getUserName', function(UserId) {
  var user = Users.findOne({
    _id: UserId
  });
  return user.profile.apellido + ', ' + user.profile.nombre;
});

// Devuelve activo o inactivo dependiendo de true vs false
Template.registerHelper('getActiveState', function(bool) {
  return (bool) ? 'Activo' : 'Inactivo';
});

// Devuelve btn-primary o btn-danger dependiendo de true vs false
Template.registerHelper('btnActiveState', function(bool) {
  return (bool) ? 'btn-primary' : 'btn-danger';
});

// Remove html tags
Template.registerHelper('removeHTMLTags', function(string) {
  return $(string).text();
});

// agregar Index al Array
Template.registerHelper('addIndexArray', function(array) {
  return _.map(array, function(value, index) {
    return {
      value: value,
      index: index
    };
  });
});
/* -----  End of Global Helpers  ------*/
