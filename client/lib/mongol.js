Session.set("Mongol", {
	'collections': ['Talleres', 'Participantes', 'Images', 'Familiares', 'Users', 'Roles', 'Clases'],
	'display': true,
	'opacity_normal': ".7",
	'opacity_expand': ".9",
	'disable_warning': 'false'
});