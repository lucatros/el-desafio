AutoForm.addHooks('FormTaller', {
  onSuccess: function(operation, result, template) {
    // Volvemos a la lista de participantes
    if (operation === 'insert') {
      Router.go('talleres.home');
      noty({
        text: 'Taller agregado'
      });
    } else if (operation === 'update') {
      Router.go('taller.info', {
        _id: this.docId
      });
      noty({
        text: 'Taller modificado'
      });
    }
  }
});

Template.tallerForm.helpers({
  tallerId: function() {
    return Iron.controller().getParams()._id;
  }
});


Template.tallerForm.rendered = function() {
  $('[data-toggle="tooltip"]').tooltip();
  $('.footable').footable();
  $(':checkbox').radiocheck();
  $('[data-toggle="switch"]').bootstrapSwitch();
};
