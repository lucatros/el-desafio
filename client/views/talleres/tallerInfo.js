Template.tallerInfo.events({
  'click .btnParticipantesEsperaDown': function(event) {
    event.preventDefault();
    var participanteIndex = this.index; // Tener en cuenta que acá el objeto solo tiene _id e index
    var tallerId = Iron.controller().getParams()._id; // Sacamos el ID de la URL
    var taller = Talleres.findOne(tallerId);
    var participantesEspera = taller.participantesEspera; // Este es el array que hay que modificar
    // splice() borra X elementos de un array en una posición dada y devuelve un array de los elementos borrados. Por eso se usa el [0], para tener el primer elemento del array
    participantesEspera[participanteIndex] = participantesEspera.splice((participanteIndex + 1), 1, participantesEspera[participanteIndex])[0];
    Talleres.update({
      _id: tallerId
    }, {
      $set: {
        participantesEspera: participantesEspera
      }
    });
  },
  'click .btnParticipantesEsperaUp': function(event) {
    event.preventDefault();
    var participanteIndex = this.index; // Tener en cuenta que acá el objeto solo tiene _id e index
    var tallerId = Iron.controller().getParams()._id; // Sacamos el ID de la URL
    var taller = Talleres.findOne(tallerId);
    var participantesEspera = taller.participantesEspera; // Este es el array que hay que modificar
    // splice() borra X elementos de un array en una posición dada y devuelve un array de los elementos borrados. Por eso se usa el [0], para tener el primer elemento del array
    participantesEspera[participanteIndex] = participantesEspera.splice((participanteIndex - 1), 1, participantesEspera[participanteIndex])[0];
    Talleres.update({
      _id: tallerId
    }, {
      $set: {
        participantesEspera: participantesEspera
      }
    });
  },
  'click .btnParticipantesEsperaBorrar': function(event) {
    event.preventDefault();
    var participanteId = this._id;
    var tallerId = Iron.controller().getParams()._id; // Sacamos el ID de la URL
    var taller = Talleres.findOne(tallerId);
    var participantesEspera = taller.participantesEspera; // Este es el array que hay que modificar

    bootbox.dialog({
      message: "¿Estás seguro de que querés borrar al participante de la lista de espera?",
      title: "Borrar de la lista de espera",
      buttons: {
        cancelar: {
          label: "Cancelar",
          className: "btn-default",
          callback: function() {
            noty({
              type: 'alert',
              text: 'El participante no se borró de la lista de espera'
            });
          }
        },
        borrar: {
          label: "Borrar",
          className: "btn-danger",
          callback: function() {
            Talleres.update(taller._id, {
              $pull: {
                participantesEspera: participanteId
              }
            });
            noty({
              type: 'warning',
              text: 'Participante borrado de la lista de espera'
            });
          }
        },
      }
    }); // end bootbox
  }
});

Template.tallerInfo.helpers({
  graphAsistenciaTaller: function() {
    // Buscamos las clases del taller
    var clases = Clases.find({
      tallerId: this._id
    }, {
      sort: {
        fecha: 1
      }
    }).fetch();

    var p = 0;
    var t = 0;
    var a = 0;
    var j = 0;

    // Buscamos las asistencias
    _.each(clases, function(clase, key, list) {
      var asistenciasClase = _.countBy(clase.participantes, function(participante, key, list) {
        return participante.asistencia;
      });
      if (asistenciasClase.P) {
        p = p + asistenciasClase.P;
      };
      if (asistenciasClase.T) {
        t = t + asistenciasClase.T;
      };
      if (asistenciasClase.A) {
        a = a + asistenciasClase.A;
      };
      if (asistenciasClase.J) {
        j = j + asistenciasClase.J;
      };
    });

    // Convertimos a porcentaje
    clasesTotales = p + t + a + j;
    p = p * 100 / clasesTotales;
    t = t * 100 / clasesTotales;
    a = a * 100 / clasesTotales;
    j = j * 100 / clasesTotales;

    var pt = p + t;
    var aj = a + j;

    var data_p = [p, t];
    var data_a = [a, j];

    var colors = [
      '#1abc9c',
      '#e74c3c',
      '#8bbc21',
      '#910000',
      '#1aadce',
      '#492970',
      '#f28f43',
      '#77a1e5',
      '#c42525',
      '#a6c96a'
    ];
    var categories = ['Presente', 'Ausente'];
    var data = [{
      y: pt,
      color: '#1abc9c',
      drilldown: {
        name: 'Total Presente',
        categories: ['Presente', 'Tardanza'],
        data: data_p,
        color: '#1abc9c'
      }
    }, {
      y: aj,
      color: '#e74c3c',
      drilldown: {
        name: 'Total Ausente',
        categories: ['Ausente', 'Justificada'],
        data: data_a,
        color: '#e74c3c'
      }
    }];

    var browserData = [];
    var versionsData = [];
    for (var i = 0; i < data.length; i++) {
      // add browser data
      browserData.push({
        name: categories[i],
        y: data[i].y,
        color: data[i].color
      });

      // add version data
      for (var j = 0; j < data[i].drilldown.data.length; j++) {
        var brightness = 0.2 - (j / data[i].drilldown.data.length) / 5;
        versionsData.push({
          name: data[i].drilldown.categories[j],
          y: data[i].drilldown.data[j],
          color: Highcharts.Color(data[i].color).brighten(brightness).get()
        });
      }
    }

    return {
      chart: {
        type: 'pie'
      },
      title: {
        text: 'Asistencias'
      },
      yAxis: {
        title: {
          text: ''
        }
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          shadow: false,
          center: ['50%', '50%']
        }
      },
      tooltip: {
        valueSuffix: '%',
        pointFormat: '<b>{point.y}</b>',
        formatter: function() {
          return this.point.name + ': <b>' + this.point.y.toFixed(2) + '</b>' + '%';
        }
      },
      series: [{
        name: 'Totales',
        data: browserData,
        size: '60%',
        dataLabels: {
          formatter: function() {
            return this.point.name + ': <b>' + this.y.toFixed(2) + '</b>' + '%';
          },
          color: 'white',
          distance: -50
        }
      }, {
        name: 'Breakdown',
        data: versionsData,
        size: '80%',
        innerSize: '70%',
        dataLabels: {
          enabled: false,
          formatter: function() {
            // display only if larger than 1
            return this.y > 1 ? '<b>' + this.point.name + ':</b> ' + this.y.toFixed(2) + '%' : null;
          }
        }
      }]
    };
  }
});
