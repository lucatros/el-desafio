Template.tallerHomeRow.helpers({
  asistenciaPTotal: function() {
    // Buscamos las clases del taller
    var clases = Clases.find({
      tallerId: this._id
    }, {
      sort: {
        fecha: 1
      }
    }).fetch();

    var p = 0;
    var t = 0;
    var a = 0;
    var j = 0;

    // Buscamos las asistencias
    _.each(clases, function(clase, key, list) {
      var asistenciasClase = _.countBy(clase.participantes, function(participante, key, list) {
        return participante.asistencia;
      });
      if (asistenciasClase.P) {
        p = p + asistenciasClase.P;
      }
      if (asistenciasClase.T) {
        t = t + asistenciasClase.T;
      }
      if (asistenciasClase.A) {
        a = a + asistenciasClase.A;
      }
      if (asistenciasClase.J) {
        j = j + asistenciasClase.J;
      }
    });

    // Convertimos a porcentaje
    clasesTotales = p + t + a + j;
    p = p * 100 / clasesTotales;
    t = t * 100 / clasesTotales;
    a = a * 100 / clasesTotales;
    j = j * 100 / clasesTotales;

    var pt = p + t;
    var aj = a + j;

    if (pt) {
      return pt.toFixed(0) + '%';
    } else {
      return '';
    }
  },
  cuposDisponibles: function() {
    if (this.participantesActivos !== null) {
      return this.cupo - this.participantesActivos.length;
    } else {
      return this.cupo;
    }
  }
});
