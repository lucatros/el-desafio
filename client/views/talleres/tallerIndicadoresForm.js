AutoForm.addHooks('FormIndicadoresTaller', {
  onSuccess: function(formType, result) {
    Router.go('taller.indicadores', {
      _id: this.docId
    });
    if (formType === 'update-pushArray') {
      noty({
        text: 'Indicador agregado al taller'
      });
    }
  }
});
