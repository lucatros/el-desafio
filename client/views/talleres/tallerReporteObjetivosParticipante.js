AutoForm.addHooks('formReportesObjetivosParticipantes', {
	before: {
		update: function(doc) {
			doc.$set.completado = true;
			return doc;
		}
	},

	onSuccess: function (formType, result) {
		Router.go('taller.reporteObjetivos', {_id: this.currentDoc.tallerId, reporteobjetivosid: this.currentDoc.reporteObjetivosId});
	}
});
