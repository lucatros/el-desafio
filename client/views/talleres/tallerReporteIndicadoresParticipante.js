AutoForm.addHooks('formReportesIndicadoresParticipantes', {
	before: {
		update: function(doc) {
			doc.$set.completado = true;
			return doc;
		}
	},

	onSuccess: function (formType, result) {
		Router.go('taller.reporteIndicadores', {_id: this.currentDoc.tallerId, reporteindicadoresid: this.currentDoc.reporteIndicadoresId});
	}
});
