Template.tallerAsistencias.rendered = function() {
  var mesActual = Session.get('mesFiltro');
  var $select = $('#select_mes').selectize();
  var selectize = $select[0].selectize;
  selectize.setValue(mesActual);
};

Template.tallerAsistencias.helpers({
  getAsistenciaParticipanteClase: function(participanteId) {
    if (this.participantes) {
      var participante = _.find(this.participantes, function(participante, key, list) {
        if (participante.id === participanteId) {
          return participante;
        } else {
          return null;
        }
      });
      if (participante) {
        return participante.asistencia;
      } else {
        return null;
      }
    }
  },
  getColorAsistencia: function(participanteId) {
    if (this.participantes) {
      var participante = _.find(this.participantes, function(participante, key, list) {
        if (participante.id === participanteId) {
          return participante;
        } else {
          return null;
        }
      });
      if (participante) {
        switch (participante.asistencia) {
          case "P":
            return 'presente';
            break;
          case "A":
            return 'ausente';
            break;
          case "T":
            return 'tardanza';
            break;
          case "J":
            return 'justificada';
            break;
          default:
            return null;
            break;
        }
      }
    }
  },
  classInactivo: function() {
    var participanteId = this._id;
    var tallerId = Iron.controller().getParams()._id;
    var taller = Talleres.findOne(tallerId);
    var clase = '';

    if (_.contains(taller.participantesInactivos, participanteId)) {
      clase = "rowInactivo";

      if (Session.get('verInactivosAsistencia')) {
        return clase;
      } else {
        return clase + ' hidden';
      }
    }
  },
  verInactivosAsistencia: function() {
    return (Session.get('verInactivosAsistencia')) ? 'checked' : '';
  },

  asistenciaTotal: function() {
    var participanteId = this._id;
    var tallerId = Iron.controller().getParams()._id;

    // Contar solo las clases del filtro
    var mesFiltro = Session.get('mesFiltro');
    if (mesFiltro === 'todo') {
      var clases = Clases.find({
        tallerId: tallerId
      }, {
        sort: {
          fecha: 1
        }
      });
    } else {
      var clases = Clases.find({
        tallerId: tallerId
      }, {
        sort: {
          fecha: 1
        }
      }).fetch();
      clases = _.filter(clases, function(clase, key, list) {
        mesClase = moment.utc(clase.fecha).month();
        return mesClase === mesFiltro;
      });
    }


    // Calcular clases totales del participante
    var clasesTotales = 0;
    clases.forEach(function(clase) {
      _.each(clase.participantes, function(participante, key, list) {
        if (participante.id === participanteId && (participante.asistencia)) {
          clasesTotales += 1;
        }
      });
    });

    // Calcular clases totales Presentes (P + T) del participante
    var clasesTotalesPresente = 0;
    clases.forEach(function(clase) {
      _.each(clase.participantes, function(participante, key, list) {
        if (participante.id === participanteId && (participante.asistencia === 'P' || participante.asistencia === 'T')) {
          clasesTotalesPresente += 1;
        }
      });
    });

    if (clasesTotales === 0) {
      return '0%';
    } else {
      var porcentaje = clasesTotalesPresente * 100 / clasesTotales;
      if (porcentaje) {
        porcentaje = porcentaje.toFixed(0) + '%';
        return porcentaje;
      } else {
        return '0%';
      }
    }
  },

  graphAsistenciaMeses: function() {
    var mesesLabel = [];
    var p = [];
    var t = [];
    var a = [];
    var j = [];
    var p_total = [];
    var clases = Clases.find({
      tallerId: this._id
    }, {
      sort: {
        fecha: 1
      }
    }).fetch();
    var cantidadClases = Clases.find({
      tallerId: this._id
    }).count();

    // buscamos los meses de las clases
    var mesesLabel = _.map(clases, function(clase, key, list) {
      return moment.utc(clase.fecha).format('MMMM');
    });
    // nos aseguramos de que no haya meses duplicados en el array
    mesesLabel = _.uniq(mesesLabel);

    // Buscamos todas las asistencias de las clases y las agregamos a un array por mes
    _.each(mesesLabel, function(mes, mesKey, list) {
      var pTotalesClases = 0;
      var tTotalesClases = 0;
      var aTotalesClases = 0;
      var jTotalesClases = 0;
      var pjTotalesClases = 0;
      _.each(clases, function(clase, key, list) {
        var mesClase = moment.utc(clase.fecha).format('MMMM');
        if (mes === mesClase) {
          // si el mes de la clase coincide con el mes del label buscamos la cantidad de P, T, A y J de la clase. Luego los agregamos al array 
          var asistenciasClase = _.countBy(clase.participantes, function(participante, key, list) {
            return participante.asistencia;
          });
          if (asistenciasClase.P) {
            pTotalesClases = pTotalesClases + asistenciasClase.P;
            pjTotalesClases = pjTotalesClases + asistenciasClase.P;
          }
          if (asistenciasClase.T) {
            tTotalesClases = tTotalesClases + asistenciasClase.T;
            pjTotalesClases = pjTotalesClases + asistenciasClase.T;
          }
          if (asistenciasClase.A) {
            aTotalesClases = aTotalesClases + asistenciasClase.A;
          }
          if (asistenciasClase.J) {
            jTotalesClases = jTotalesClases + asistenciasClase.J;
          }
        }
      });
      p[mesKey] = pTotalesClases;
      t[mesKey] = tTotalesClases;
      a[mesKey] = aTotalesClases;
      j[mesKey] = jTotalesClases;
      p_total[mesKey] = pjTotalesClases;
    }); // End _each de mesesLabel

    // Convertimos a porcentaje
    _.each(mesesLabel, function(mes, i, list) {
      var total_clases_mes = p[i] + t[i] + a[i] + j[i];
      p[i] = p[i] * 100 / total_clases_mes;
      t[i] = t[i] * 100 / total_clases_mes;
      a[i] = a[i] * 100 / total_clases_mes;
      j[i] = j[i] * 100 / total_clases_mes;
      p_total[i] = p[i] + t[i];
    });

    return {
      title: {
        text: ''
      },
      xAxis: {
        categories: mesesLabel,
        title: {
          text: ''
        }
      },
      yAxis: {
        title: {
          text: ''
        }
      },

      labels: {},
      tooltip: {
        formatter: function() {
          // return '<b>'+ this.series.name +'</b>: '+ Math.round(this.percentage) +' %';
          return '<span style="color:' + this.series.color + '">\u25CF</span> ' + this.series.name + ': <b>' + this.point.y.toFixed(0) + '%</b><br/>';
        }

        // pointFormat: function() {
        //  // return '<span style="color:'+ series.color +'">\u25CF</span>'+ series.name +': <b>'+point.y+'%</b><br/>';
        //  return '<span style="color:{series.color}">\u25CF</span> {series.name}: <b>{point.y}%</b><br/>'
        // }
      },

      series: [{
        type: 'column',
        name: 'Presente',
        color: '#7CC576',
        data: p
      }, {
        type: 'column',
        name: 'Tardanza',
        color: '#A2D39C',
        data: t
      }, {
        type: 'column',
        name: 'Ausente',
        color: '#F26C4F',
        data: a
      }, {
        type: 'column',
        name: 'Justificada',
        color: '#F7977A',
        data: j
      }, {
        type: 'spline',
        name: 'Presente Total',
        data: p_total,
        marker: {
          lineWidth: 2,
          lineColor: '#7CC576',
          fillColor: '#7CC576',
          color: '#7CC576'
        },
        dataLabels: {
          enabled: true,
          formatter: function() {
            var pcnt = this.y;
            return Highcharts.numberFormat(pcnt, 0) + '%';
          },
          y: -10

        },

      }]
    };
  }
});

Template.tallerAsistencias.events({
  'click #borrarClase': function(event) {
    event.preventDefault();
    var clase = this;
    bootbox.dialog({
      message: "¿Estás seguro de que querés borrar esta clase?",
      title: "Borrar Clase",
      buttons: {
        cancelar: {
          label: "Cancelar",
          className: "btn-default",
          callback: function() {
            noty({
              type: 'warning',
              text: 'Clase no borrada'
            });
          }
        },
        agregar: {
          label: "Borrar",
          className: "btn-danger",
          callback: function() {
            Clases.remove(clase._id);
            noty({
              type: 'error',
              text: 'Clase borrada'
            });
          }
        },
      }
    }); // end bootbox
  },
  'change #select_mes': function(event) {
    var mesSelected = $('#select_mes').val();
    Session.set('mesFiltro', mesSelected);
  },
  'click .btn-filtros': function(event) {
    event.preventDefault();
    if ($('.filters-wrapper').is(':hidden')) {
      $('.filters-wrapper').slideDown('slow');
    } else {
      $('.filters-wrapper').slideUp('slow');
    }
  },
  'change #verInactivosAsistencia': function(event) {
    event.preventDefault();
    var verInactivosAsistencia = Session.get('verInactivosAsistencia');

    if (verInactivosAsistencia) {
      Session.set('verInactivosAsistencia', false);
    } else {
      Session.set('verInactivosAsistencia', true);
    }
  },
});
