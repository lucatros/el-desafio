Template.tallerInfoAside.helpers({
  cantidadReportesACompletar: function() {
    var tallerId = Iron.controller().getParams()._id;

    // Contamos cantidad de reportes por indicadores a completar
    Meteor.call("cantidadReportesIndicadoresACompletar", tallerId, function(error, data) {
      if (error) {
        console.log(error);
      }
      Session.set('cantidadReportesIndicadoresACompletar', data);
    });

    // Contamos cantidad de reportes por objetivos a completar
    Meteor.call("cantidadReportesObjetivosACompletar", tallerId, function(error, data) {
      if (error) {
        console.log(error);
      }
      Session.set('cantidadReportesObjetivosACompletar', data);
    });

    var i = Session.get('cantidadReportesIndicadoresACompletar');
    var o = Session.get('cantidadReportesObjetivosACompletar');
    var total = i + o;

    Session.set('cantidadReportesACompletar', total);

    return Session.get('cantidadReportesACompletar');
  }
});
