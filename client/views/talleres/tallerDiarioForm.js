AutoForm.addHooks('FormDiarioTaller', {
  // before: {
  //  update: function (doc) {
  //    console.log(doc);
  //  }
  // },
  // onSubmit: function(insertDoc, updateDoc, currentDoc) {
  //     // You must call this.done()!
  //     console.log(insertDoc);
  //     console.log(updateDoc);
  //     console.log(currentDoc);
  //     // this.done(); // submitted successfully, call onSuccess
  //     // this.done(new Error('foo')); // failed to submit, call onError with the provided error
  //     // this.done(null, "foo"); // submitted successfully, call onSuccess with `result` arg set to "foo"
  //   },

  onSuccess: function(formType, result) {
    Router.go('taller.diario', {
      _id: this.docId
    });
    if (formType === 'update-pushArray') {
      noty({
        text: 'Evento agregado al diario'
      });
    } else if (formType === 'update') {
      noty({
        text: 'Evento modificado correctamente'
      });
    }
  }
});
