Template.tallerReporteIndicadores.events({
  'click .marcarReporteIndicadoresCompletado': function() {
    var reporteIndicadoresId = Iron.controller().getParams().reporteindicadoresid;
    var tallerId = Iron.controller().getParams()._id;
    var participantesSinCompletar = ReportesIndicadoresParticipantes.find({
      tallerId: tallerId,
      reporteIndicadoresId: reporteIndicadoresId,
      completado: {
        $ne: true
      }
    }).count();
    if (participantesSinCompletar > 0) {
      bootbox.dialog({
        message: "¡ATENCIÓN! Aún quedan reportes sin completar para alguno de los participantes. Si se marca el reporte como completado, estos participantes no serán tenidos en cuenta en el reporte final. ¿Seguro que querés marcar el reporte como completado?",
        title: "Marcar reporte como completado",
        buttons: {
          cancelar: {
            label: "Cancelar",
            className: "btn-default",
            callback: function() {
              noty({
                type: 'warning',
                text: 'El reporte NO se marcó como completado'
              });
            }
          },
          agregar: {
            label: "Completar",
            className: "btn-primary",
            callback: function() {
              Meteor.call('marcarReporteIndicadoresCompletado', tallerId, reporteIndicadoresId);
              noty({
                text: 'Reporte marcado como completado'
              });
              Router.go('taller.reportes', {
                _id: tallerId
              });
            }
          },
        }
      }); // end bootbox
    } else {
      bootbox.dialog({
        message: "¿Estás seguro de que querés marcar este reporte como completado?",
        title: "Marcar reporte como completado",
        buttons: {
          cancelar: {
            label: "Cancelar",
            className: "btn-default",
            callback: function() {
              noty({
                type: 'warning',
                text: 'El reporte NO se marcó como completado'
              });
            }
          },
          agregar: {
            label: "Completar",
            className: "btn-primary",
            callback: function() {
              Meteor.call('marcarReporteIndicadoresCompletado', tallerId, reporteIndicadoresId);
              noty({
                text: 'Reporte marcado como completado'
              });
              Router.go('taller.reportes', {
                _id: tallerId
              });
            }
          },
        }
      }); // end bootbox
    }
  }
});
