AutoForm.addHooks('FormAsistenciaTaller', {
  before: {
    insert: function(doc) {
      // Antes de agregar la clase, agregamos el ID del taller
      doc.tallerId = Session.get('claseTallerId');
      return doc;
    }
  },
  onSuccess: function(formType, result) {
    var tallerId = Session.get('claseTallerId');

    if (formType === 'insert') {
      bootbox.dialog({
        message: "¿Querés agregar una entrada al diario del taller con información sobre esta clase?<br>En el diario podés contar como se desarrollo la clase, si hubo algún problema, algo bueno para destacar, etc. No es obligatorio agregar un evento ahora, pero es recomendable.",
        title: "Agregar envento al diario",
        buttons: {
          cancelar: {
            label: "No agregar evento",
            className: "btn-default",
            callback: function() {
              Router.go('taller.asistencias', {
                _id: tallerId
              });
              noty({
                text: 'Clase agregada'
              });
            }
          },
          agregar: {
            label: "Agregar evento",
            className: "btn-primary",
            callback: function() {
              Router.go('taller.diarioForm', {
                _id: tallerId
              });
              noty({
                text: 'Clase agregada'
              });
            }
          },
        }
      }); // end bootbox
    } else {
      Router.go('taller.asistencias', {
        _id: tallerId
      });
      noty({
        text: 'Clase modificada'
      });
    }
  }
});

Template.tallerAsistenciasForm.rendered = function() {
  $('input[participanteIdInput]').each(function() {
    var id = $(this).attr('participanteIdInput');
    $(this).val(id);
  });
};

Template.tallerAsistenciasForm.helpers({
  classInactivo: function() {
    var participanteId = this._id;
    var tallerId = Iron.controller().getParams()._id;
    var taller = Talleres.findOne(tallerId);
    var clase = '';

    if (_.contains(taller.participantesInactivos, participanteId)) {
      clase = "rowInactivo";

      if (Session.get('verInactivosAsistencia')) {
        return clase;
      } else {
        return clase + ' hidden';
      }
    }
  }
});
