Template.staffHome.rendered = function() {
  $('[data-toggle="tooltip"]').tooltip();
  $('.footable').footable();
  // Focus state for append/prepend inputs
  $('.input-group').on('focus', '.form-control', function() {
    $(this).closest('.input-group, .form-group').addClass('focus');
  }).on('blur', '.form-control', function() {
    $(this).closest('.input-group, .form-group').removeClass('focus');
  });
};

Template.staffHome.helpers({
  // checkIfVolTallerista: function () {
  //  var u = this;
  //  console.log(u);
  //  return Meteor.call('checkifuserhas', u, function (error, result) {
  //    console.log(result);
  //  });
  // }
});
