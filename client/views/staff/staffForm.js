AutoForm.addHooks('FormUser', {
  onError: function(formType, error) {
    console.log(error);
    if (error.reason) {
      var t = error.reason;
      if (t.indexOf('index: meteor.users.$emails.') >= 0) {
        noty({
          type: 'error',
          text: 'Ya existe un usuario con ese mail.'
        });
      }
    }
  },

  // Called every time the form is revalidated, which can be often if keyup
  // validation is used.

  onSuccess: function(operation, result, template) {
    // Si la operación es INSERT, enviamos el mail de Enrollment
    if (operation === 'insert') {
      var formUserId = result;

      Meteor.call('enviarMailNuevoUsuario', formUserId, function(error, result) {});
    }

    // If a new image was added, we add it to the Collection
    if (Session.get('newAvatar') === true) {
      var imageTempBase64 = Session.get('imageAvatar');
      if (operation === 'insert') {
        var formUserId = result;
      }
      // Si es un update, asignamos el userId y borramos la imagen vieja
      if (operation === 'update') {
        var formUserId = this.docId;
        var user = Users.findOne({
          _id: formUserId
        });
        oldImageId = user.foto;
        Images.remove({
          _id: oldImageId
        });
      }

      var newFile = new FS.File(imageTempBase64);

      // Insert the image in the Images Collection and asign the _id to a variable
      imageId = Images.insert(newFile, function(err, fileObj) {});

      // Cambiamos .foto del documento insertado al id de la imagen
      Users.update({
        _id: formUserId
      }, {
        $set: {
          'profile.foto': imageId._id
        }
      });
    }

    // Volvemos a la lista de participantes
    if (operation === 'insert') {
      Router.go('staff.home');
      noty({
        text: 'Miembro del staff agregado'
      });
    } else if (operation === 'update') {
      // Router.go('user.info', {_id: this.docId});
      Router.go('staff.home');
      noty({
        text: 'Miembro del staff modificado'
      });
    }
  }
});

Template.staffForm.events({
  'click .cancelar': function(event) {
    event.preventDefault();
    var usuarioId = Iron.controller().getParams()._id;
    if (usuarioId) {
      Router.go('user.info', {
        _id: usuarioId
      });
    } else {
      Router.go('staff.home');
    }
  }
});

Template.staffForm.rendered = function() {
  // La variable imageAvatar es para la imagen del user.
  Session.set('imageAvatar', '/images/profile_picture.png');
  Session.set('avatarChanged', false);
  // The newAvatar variable is set to true or false. Depending on its value, we will or won't add the img ource to the Images Collection. Por defecto es 'false'.
  Session.set('newAvatar', false);
  $(':checkbox').radiocheck();
  $('[data-toggle="radio"]').radiocheck();
};
