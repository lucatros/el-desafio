Template.participanteFamiliares.rendered = function() {
  Meteor.typeahead.inject();
};

AutoForm.addHooks('formFamiliar', {
  onSuccess: function(operation, result, template) {
    // Router.go('participante.medicos', {_id: this.docId});
    // noty({text: 'Se envió el form'});
  }
});

Template.participanteFamiliares.helpers({
  familiaresTypeahead: function() {
    return [{
      name: 'familiares',
      valueKey: 'value',
      local: function() {
        return Familiares.find().fetch().map(function(it) {
          return {
            id: it._id,
            value: it.apellido + ', ' + it.nombre
          };
        });
      },
      header: '<p class="typeaheadHeading">Familiares</p>'
    }, {
      name: 'participantes',
      valueKey: 'value',
      local: function() {
        return Participantes.find().fetch().map(function(it) {
          return {
            id: it._id,
            value: it.apellido + ', ' + it.nombre
          };
        });
      },
      header: '<p class="typeaheadHeading">Participantes</p>'
    }];
  },
  familiares: function() {
    var participanteId = Iron.controller().getParams()._id;
    var participante = Participantes.findOne(participanteId);
    return participante.familiares;
  },
  selected: function(event, suggestion, datasetName) {
    $('#row_relacion').show();
    Session.set('familiarId', suggestion.id);
  },

  getApellidoNombre: function(id) {
    var participante = Participantes.findOne(id);
    var familiar = Familiares.findOne(id);

    if (familiar) {
      return familiar.apellido + ', ' + familiar.nombre;
    }

    if (participante) {
      return participante.apellido + ', ' + participante.nombre;
    }
  },
  esParticipante: function(id) {
    var participante = Participantes.findOne(id);
    return participante ? true : false;
  },
  esInscripcion: function() {
    return Session.get('esInscripcion');
  }

});

Template.participanteFamiliares.events({
  'click .cancelarRelacion': function(event) {
    event.preventDefault();
    $('.error-relacion').hide();
    $('.typeahead').typeahead('val', '');
    $('#row_relacion').hide();
    $('#relacion').val('');
    $('#emergencia').radiocheck('uncheck');
  },
  'click .agregarRelacion': function(event) {
    event.preventDefault();
    var relacion = $('#relacion').val();
    var emergencia = $('#emergencia').is(":checked");
    console.log('Relación: ' + relacion);

    // Si no se introdujo la relación, mostramos el error
    if (relacion === '') {
      $('.error-relacion').show();
      return false;
    }

    // Si se introdujo la relación, agregamos la relación al Participante y reseteamos los campos
    var familiarId = Session.get('familiarId');
    var participanteId = Iron.controller().getParams()._id;

    Participantes.update(participanteId, {
      $addToSet: {
        familiares: {
          id: familiarId,
          relacion: relacion,
          emergencia: emergencia
        }
      }
    });

    $('.error-relacion').hide();
    $('.typeahead').typeahead('val', '');
    $('#row_relacion').hide();
    $('#relacion').val('');
    $('#emergencia').radiocheck('uncheck');
    noty({
      text: 'Relación del participante creada'
    });
  },
  // 'click .agregarFamiliar': function (event, template) {
  //  event.preventDefault();
  //  Session.set('formFamiliarAction', 'insert');
  //  Session.set('familiar', null);
  //  bootbox.dialog({ // https://stackoverflow.com/questions/22418592/bootboxjs-how-to-render-a-meteor-template-as-dialog-body
  //    className: "modal_form_familiar",
  //    message: "<div id='dialogNode'></div>",
  //    title: "Agregar datos personales del familiar",
  //    buttons: {
  //      cancelar: {
  //        label: "Cancelar",
  //        className: "btn-default",
  //        callback: function() {
  //          noty({type: 'alert', text: 'No se agregó al familiar'});
  //          AutoForm.resetForm('formFamiliar');
  //        }
  //      },
  //      agregar: {
  //        label: "Agregar",
  //        className: "btn-primary",
  //        callback: function() {
  //          if (AutoForm.validateForm('formFamiliar')) {
  //            $("#formFamiliar").submit();
  //            noty({text: 'Familiar agregado. Buscarlo en la lista'});
  //            return true;
  //          } else {
  //            return false;
  //          }
  //        }
  //      }
  //    }
  //  });
  //  Blaze.render(Template.participanteFamiliaresForm,$("#dialogNode")[0]);
  // },

  'click .borrarRelacion': function(event, template) {
    event.preventDefault();
    var familiarId = this.id;
    var participanteId = Iron.controller().getParams()._id;
    console.log(this);

    bootbox.dialog({
      message: "¿Estás seguro de borrar la relación entre el familiar y el participante?",
      title: "Borrar relación",
      buttons: {
        cancelar: {
          label: "Cancelar",
          className: "btn-default",
          callback: function() {
            noty({
              type: 'alert',
              text: 'No se borró la relación'
            });
          }
        },
        borrar: {
          label: "Borrar",
          className: "btn-danger",
          callback: function() {
            Participantes.update(participanteId, {
              $pull: {
                familiares: {
                  id: familiarId
                }
              }
            });
            noty({
              type: 'error',
              text: 'Relación borrada'
            });
          }
        },
      }
    }); // end bootbox
  },
  'click .btnFinalizar': function(event) {
    event.preventDefault();
    Session.set('esInscripcion', false);
    Router.go('participantes.home');
    noty({
      text: '¡Inscripción finalizada!'
    });
  }
});
