Template.participanteInfo.events({
  'click .btnParticipanteActivo': function(event) {
    event.preventDefault();
    var currentState = this.activo;
    var newState = !currentState;
    participanteId = this._id;
    var user = Meteor.user();
    if (_.contains(user.roles, 'admin')) {
      bootbox.dialog({
        message: "¿Estás seguro de que querés cambiar el estado del participante? (En caso de marcarlo como inactivo, se va a marcar como inactivo en todos los talleres)",
        title: "Cambiar estado del participante",
        buttons: {
          cancelar: {
            label: "Cancelar",
            className: "btn-default",
            callback: function() {
              // noty({type: 'warning', text: '__text__'});
            }
          },
          cambiar: {
            label: "Cambiar",
            className: "btn-primary",
            callback: function() {
              Meteor.call('marcarParticipanteInactivo', participanteId);
              Participantes.update({
                _id: participanteId
              }, {
                $set: {
                  activo: newState
                }
              });
              noty({
                text: 'Se cambió el estado del participante'
              });
            }
          },
        }
      }); // end bootbox
    } else {
      bootbox.dialog({
        message: "No tenés permisos para cambiar el estado del participante.",
        title: "Cambiar estado del participante",
        buttons: {
          cancelar: {
            label: "Cancelar",
            className: "btn-default",
            callback: function() {
              // noty({type: 'warning', text: '__text__'});
            }
          }
        }
      }); // end bootbox
    }
  }
});

Template.participanteInfo.helpers({
  graphAsistenciaParticipante: function() {
    // Buscamos las clases del taller
    var clases = Clases.find().fetch();
    var participanteId = this._id;
    var asistenciasParticipante = [];

    _.each(clases, function(clase, key1, list1) {
      _.each(clase.participantes, function(participante, key2, list2) {
        if (participante.id === participanteId) {
          asistenciasParticipante.push(participante.asistencia);
        }
      });
    });

    var p = 0;
    var t = 0;
    var a = 0;
    var j = 0;

    // Buscamos las asistencias
    for (var i = 0; i < asistenciasParticipante.length; i++) {
      if (asistenciasParticipante[i] === 'P') {
        p++;
      }
      if (asistenciasParticipante[i] === 'T') {
        t++;
      }
      if (asistenciasParticipante[i] === 'A') {
        a++;
      }
      if (asistenciasParticipante[i] === 'J') {
        j++;
      }
    }

    // Convertimos a porcentaje
    clasesTotales = p + t + a + j;
    p = p * 100 / clasesTotales;
    t = t * 100 / clasesTotales;
    a = a * 100 / clasesTotales;
    j = j * 100 / clasesTotales;

    var pt = p + t;
    var aj = a + j;

    var data_p = [p, t];
    var data_a = [a, j];

    var colors = [
      '#1abc9c',
      '#e74c3c',
      '#8bbc21',
      '#910000',
      '#1aadce',
      '#492970',
      '#f28f43',
      '#77a1e5',
      '#c42525',
      '#a6c96a'
    ];
    var categories = ['Presente', 'Ausente'];
    var data = [{
      y: pt,
      color: '#1abc9c',
      drilldown: {
        name: 'Total Presente',
        categories: ['Presente', 'Tardanza'],
        data: data_p,
        color: '#1abc9c'
      }
    }, {
      y: aj,
      color: '#e74c3c',
      drilldown: {
        name: 'Total Ausente',
        categories: ['Ausente', 'Justificada'],
        data: data_a,
        color: '#e74c3c'
      }
    }];

    var browserData = [];
    var versionsData = [];
    for (var i = 0; i < data.length; i++) {
      // add browser data
      browserData.push({
        name: categories[i],
        y: data[i].y,
        color: data[i].color
      });

      // add version data
      for (var j = 0; j < data[i].drilldown.data.length; j++) {
        var brightness = 0.2 - (j / data[i].drilldown.data.length) / 5;
        versionsData.push({
          name: data[i].drilldown.categories[j],
          y: data[i].drilldown.data[j],
          color: Highcharts.Color(data[i].color).brighten(brightness).get()
        });
      }
    }

    return {
      chart: {
        type: 'pie'
      },
      title: {
        text: 'Asistencias'
      },
      yAxis: {
        title: {
          text: ''
        }
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          shadow: false,
          center: ['50%', '50%']
        }
      },
      tooltip: {
        valueSuffix: '%',
        pointFormat: '<b>{point.y}</b>',
        formatter: function() {
          return this.point.name + ': <b>' + this.point.y.toFixed(2) + '</b>' + '%';
        }
      },
      series: [{
        name: 'Totales',
        data: browserData,
        size: '60%',
        dataLabels: {
          formatter: function() {
            return this.point.name + ': <b>' + this.y.toFixed(2) + '</b>' + '%';
          },
          color: 'white',
          distance: -50
        }
      }, {
        name: 'Breakdown',
        data: versionsData,
        size: '80%',
        innerSize: '70%',
        dataLabels: {
          enabled: false,
          formatter: function() {
            // display only if larger than 1
            return this.y > 1 ? '<b>' + this.point.name + ':</b> ' + this.y.toFixed(2) + '%' : null;
          }
        }
      }]
    };
  }
});
