AutoForm.addHooks('FormBiografia', {
	onSuccess: function (formType, result) {
		Router.go('participante.biografia', {_id: this.docId});
		noty({text: 'Biografía actualizada'});	
	}
});
