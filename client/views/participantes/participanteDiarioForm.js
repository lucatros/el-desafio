AutoForm.addHooks('FormDiarioParticipante', {
  onSuccess: function(formType, result) {
    Router.go('participante.diario', {
      _id: this.docId
    });
    if (formType === 'update-pushArray') {
      noty({
        text: 'Evento agregado al diario'
      });
    } else if (formType === 'update') {
      noty({
        text: 'Evento modificado correctamente'
      });
    }
  }
});
