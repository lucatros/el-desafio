AutoForm.addHooks('FormParticipanteMedico', {
  onSuccess: function(operation, result, template) {
    if (Session.get('esInscripcion') === true) {
      Router.go('participante.talleres', {
        _id: this.docId
      });
      noty({
        text: 'Datos médicos ingresados correctamente'
      });
      // Session.set('esInscripcion', false);
    } else {
      Router.go('participante.medicos', {
        _id: this.docId
      });
      noty({
        text: 'Datos médicos modificados correctamente'
      });
      // Session.set('esInscripcion', false);
    }
  }
});
