Template.participanteTalleresEach.helpers({
  checkboxState: function() {
    // Sacamos el _id del participante de la URL usando Iron.controller
    var participanteId = Iron.controller().getParams()._id;
    var participante = Participantes.findOne({
      _id: participanteId
    });
    var taller = this;

    // Note: Months are zero indexed, so January is month 0
    var jun30 = moment.utc({
      month: 5,
      day: 30
    });
    var dec31 = moment.utc({
      month: 11,
      day: 31
    });
    var fechaNacimiento = moment.utc(participante.fechaNacimiento);
    edadAlJun30 = jun30.diff(fechaNacimiento, 'years');
    edadAlDec30 = dec31.diff(fechaNacimiento, 'years');

    // Ver si el sexo corresponde con el del taller
    if ((participante.sexo === taller.sexo) || (taller.sexo === 'mixto')) {
      var sexoValido = true;
    } else {
      var sexoValido = false;
    }

    // Primero vemos si tenemos que calcular la edad al 30 de junio o solo teniendo en cuenta el año y a partir de ahi habilitamos o deshabilitamos la checkbox
    if (taller.edadAnoCalendario) {
      if (!(edadAlDec30 >= taller.edadDesde && edadAlDec30 <= taller.edadHasta) || (sexoValido === false)) {
        return 'disabled';
      } else {
        return '';
      }
    } else {
      // Si no tiene la edad o si no tiene el sexo correspondiente.
      // EL cupo no se usa para "disabled" porque si no marca como disabled cuando el participante está activo en el taller y no quedan cupos
      if (!(edadAlJun30 >= taller.edadDesde && edadAlJun30 <= taller.edadHasta) || (sexoValido === false)) {
        return 'disabled';
      } else {
        return '';
      }
    }
  },
  checkedOnOff: function() {
    var participanteId = Iron.controller().getParams()._id;
    var participante = Participantes.findOne({
      _id: participanteId
    });
    var taller = this;
    if (_.contains(taller.participantesActivos, participanteId) === true) {
      return 'checked';
    } else {
      return '';
    }
  },
  cuposDisponibles: function() {
    participantesActivos = this.participantesActivos; // this es el Taller
    if (participantesActivos !== null) {
      return this.cupo - participantesActivos.length;
    } else {
      return this.cupo;
    }
  },
  cuposDisponiblesClass: function() {
    participantesActivos = this.participantesActivos; // this es el Taller
    if (participantesActivos !== null) {
      var cuposDisponibles = this.cupo - participantesActivos.length;
    } else {
      var cuposDisponibles = this.cupo;
    }
    return (cuposDisponibles <= 2) ? 'text-danger' : '';
  }
});

Template.participanteTalleresEach.events({
  // El click tiene que asociarse al label, sino no funciona.
  'click .checkbox': function(event) {
    event.preventDefault();
    // creamos la variable checkbox para poder cambiarla más adelante
    var label = event.currentTarget;
    var checkbox = $(label).find('input:checkbox');

    // Calcular cupos disponibles
    participantesActivos = this.participantesActivos; // this es el Taller
    if (participantesActivos !== null) {
      var cuposDisponibles = this.cupo - participantesActivos.length;
    } else {
      var cuposDisponibles = this.cupo;
    }

    // Solo procesamos el click si no tiene la clase disabled
    if (!$(label).hasClass('disabled')) {
      // creamos las variables para el taller y le participanteId
      var taller = this;
      var participanteId = Iron.controller().getParams()._id;

      // Si estaba chequeado, preguntamos si se está seguro en marcarlo como inactivo
      if ($(checkbox).is(':checked')) {
        bootbox.dialog({
          message: "El participante va a ser marcado como inactivo en el taller de " + taller.nombre + ". ¿Éstás seguro?",
          title: "Marcar como inactivo",
          buttons: {
            cancelar: {
              label: "Cancelar",
              className: "btn-default",
              callback: function() {
                noty({
                  type: 'warning',
                  text: 'El participante sigue estando activo en el taller de ' + taller.nombre + ''
                });
              }
            },
            agregar: {
              label: "Marcar como inactivo",
              className: "btn-primary",
              callback: function() {
                // Borramos al participante de la lista de activos y lo pasamos a inactivos
                Talleres.update(taller._id, {
                  $pull: {
                    participantesActivos: participanteId
                  }
                });
                Talleres.update(taller._id, {
                  $addToSet: {
                    participantesInactivos: participanteId
                  }
                });
                checkbox.radiocheck('uncheck');
                noty({
                  type: 'warning',
                  text: 'Participante marcado como inactivo en el taller de ' + taller.nombre + ''
                });
              }
            },
          }
        }); // end bootbox
        // Si no estaba chequeado, lo agregamos al taller, solo si hay cupos disponibles
      } else if (cuposDisponibles > 0) {
        bootbox.dialog({
          message: "¿Estás segur@ de que querés agregar al participante al taller de " + taller.nombre + "?",
          title: "Agregar participante al taller",
          buttons: {
            cancelar: {
              label: "Cancelar",
              className: "btn-default",
              callback: function() {
                noty({
                  type: 'warning',
                  text: 'No se agregó al participante'
                });
              }
            },
            agregar: {
              label: "Agregar",
              className: "btn-primary",
              callback: function() {
                Talleres.update(taller._id, {
                  $addToSet: {
                    participantesActivos: participanteId
                  }
                });
                Talleres.update(taller._id, {
                  $pull: {
                    participantesInactivos: participanteId
                  }
                });
                Talleres.update(taller._id, {
                  $pull: {
                    participantesEspera: participanteId
                  }
                });
                checkbox.radiocheck('check');
                noty({
                  text: 'Participante agregado al taller de ' + taller.nombre + ''
                });
              }
            },
          }
        }); // end bootbox
      // Si no hay cupos, se le ofrece agregarlo a la lista de espera (TODO)
      } else {
        bootbox.dialog({
          message: "No hay cupos disponibles para el taller de " + taller.nombre + ". ¿Querés agregar al participante a la lista de espera?",
          title: "No hay cupos disponibles",
          buttons: {
            cancelar: {
              label: "Cancelar",
              className: "btn-default",
              callback: function() {
                noty({
                  type: 'warning',
                  text: 'No se agregó al participante a la lista de espera'
                });
              }
            },
            agregar: {
              label: "Agregar",
              className: "btn-info",
              callback: function() {
                Talleres.update(taller._id, {
                  $addToSet: {
                    participantesEspera: participanteId
                  }
                });
                Talleres.update(taller._id, {
                  $pull: {
                    participantesInactivos: participanteId
                  }
                });
                noty({
                  text: 'Agregado!'
                });
              }
            },
          }
        }); // end bootbox
      } // end else
    } // end if has class disabled
  } // end click
});
