Template.participanteForm.onCreated(function() {
  // We create a reactive dict var for the pageSession (temporary vars for the page)
  // We avoid using Sessions so we don't polute the Global scope
  this.pageSession = new ReactiveDict();

  const _id = FlowRouter.getParam('_id');

  // If :_id on the URL is "new" it means it's an insert form
  if (_id === 'new') {
    this.pageSession.set('formType', 'insert');
  }

  // If :_id matches an actual participante _id, it's an update form

  // If :_id exists but doesn't match any participante, got o nonFound
  if (_id === 'XXXX') {
    this.pageSession.set('formType', 'XXXX');
  }
});


Template.participanteForm.helpers({
  formType() {
    return Template.instance().pageSession.get('formType');
  },
  botonFormParticipante() {
    const _id = FlowRouter.getParam('_id');
    return (_id === 'new') ? 'Agregar' : 'Modificar';
  },
  isUploading() {
    return Session.get('isUploading');
  }
});





AutoForm.addHooks('form-participante', {
  onSuccess: function(operation, result, template) {
    // If a new image was added, we add it to the Collection
    if (Session.get('newAvatar') === true) {
      var imageTempBase64 = Session.get('imageAvatar');
      if (operation === 'insert') {
        var ParticipanteId = result;
      }
      // Si es un update, asignamos el ParticipanteId y borramos la imagen vieja
      if (operation === 'update') {
        var ParticipanteId = this.docId;
        var participante = Participantes.findOne({
          _id: ParticipanteId
        });
        oldImageId = participante.foto;
        Images.remove({
          _id: oldImageId
        });
      }

      var newFile = new FS.File(imageTempBase64);

      // Insert the image in the Images Collection and asign the _id to a variable
      imageId = Images.insert(newFile, function(err, fileObj) {});

      // Cambiamos .foto del documento insertado al id de la imagen
      Participantes.update({
        _id: ParticipanteId
      }, {
        $set: {
          foto: imageId._id
        }
      });
    }

    // Seguimos a los datos medicos o volvemos al participante
    if (operation === 'insert') {
      Router.go('participante.medicosForm', {
        _id: result
      });
      noty({
        text: 'Participante agregado'
      });
      Session.set('esInscripcion', true);
    } else if (operation === 'update') {
      Router.go('participante.info', {
        _id: this.docId
      });
      noty({
        text: 'Participante modificado'
      });
    }
  }
});
