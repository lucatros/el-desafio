Template.onRendered(function() {
  //  Initialize all datepicker inputs whenever any template is rendered
  this.$('.datepicker').datepicker();
  $('[data-toggle="tooltip"]').tooltip();
  $('.footable').footable();
  $(':checkbox').radiocheck();
  $('[data-toggle="switch"]').bootstrapSwitch();
  $('#selectTalleresReporteObjetivos').selectize();
  $('.note-editor').dropdown();
  $('.dropdown-toggle').dropdown();
  //  Focus state for append/prepend inputs
  $('.input-group').on('focus', '.form-control', function() {
    $(this).closest('.input-group, .form-group').addClass('focus');
  }).on('blur', '.form-control', function() {
    $(this).closest('.input-group, .form-group').removeClass('focus');
  });
});

// AutoForm.debug();
// //  Solución temporal para autoform ver: https://github.com/aldeed/meteor-autoform/issues/751
// Template.autoForm.hooks({
//   rendered: function() {
//     if (this.data.type === 'insert') {
//       AutoForm.getValidationContext(this.data.id).resetValidation();
//       AutoForm.resetForm(this.data.id);
//       //  There are some elements that doesn't properly reset with above command so they have to be manually cleared.
//       //  I'm just showing the ones I have to manually clear on my particular case, so test all elements on your form to ensure they are cleared.

//       //  Selectize elements
//       var $select = $('select').selectize();
//       for (var i = 0; i < $select.length; i++) {
//         $select[i].selectize.clear();
//       }

//       //  Datepickers
//       var allDatePickers = $('[data-out-mode=utcDate]');
//       for (var i = 0; i < allDatePickers.length; i++) {
//         allDatePickers.eq(i).data("DateTimePicker").date(null);
//       }

//       // Timepicker
//       var allTimePickers = $('[data-out-mode=utcDateTime]');
//       for (var i = 0; i < allTimePickers.length; i++) {
//         allTimePickers.eq(i).data("DateTimePicker").date(null);
//       }

//       // Botones de las asistencias
//       $('.botonesAsistencia').closest('label').removeClass("active");
//       $('.botonesAsistencia').closest('label').removeClass("active").children("input").removeAttr("checked");
//     } else if (this.data.type === 'update') {
//       // Do something
//     }
//   }
// });
