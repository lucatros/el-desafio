AutoForm.addHooks('formFamiliar', {
	onSuccess: function (operation, result) {
		var backToParticipante = Session.get('backToParticipante');
		if (backToParticipante) {
			Router.go('participante.familiares', {_id: backToParticipante});
		} else {
			Router.go('familiar.info', {_id: this.docId});
		}
		noty({text: 'Datos del familiar modificados'});		
	}
});

Template.familiarForm.helpers({
	familiarId: function () {
		return Iron.controller().getParams()._id;
	}
});

Template.familiarForm.events({

});

