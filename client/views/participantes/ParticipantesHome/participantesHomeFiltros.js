Template.participantesHomeFiltros.helpers({
  checkboxInactivos() {
    return (Session.get('incluirInactivos')) ? 'checked' : '';
  },
  verColumnaFoto() {
    return (Session.get('verColumnaFoto')) ? 'checked' : 'unchecked';
  },
  verColumnaEdad() {
    return (Session.get('verColumnaEdad')) ? 'checked' : 'unchecked';
  },
  verColumnaEscuela() {
    return (Session.get('verColumnaEscuela')) ? 'checked' : 'unchecked';
  },
  verColumnaGradoAno() {
    return (Session.get('verColumnaGradoAno')) ? 'checked' : 'unchecked';
  },
  verColumnaSexo() {
    return (Session.get('verColumnaSexo')) ? 'checked' : 'unchecked';
  },
  verColumnaDNI() {
    return (Session.get('verColumnaDNI')) ? 'checked' : 'unchecked';
  },
  verColumnaEscolarizado() {
    return (Session.get('verColumnaEscolarizado')) ? 'checked' : 'unchecked';
  },
  verColumnaEstrellas() {
    return (Session.get('verColumnaEstrellas')) ? 'checked' : 'unchecked';
  },
  verColumnaDocumentos() {
    return (Session.get('verColumnaDocumentos')) ? 'checked' : 'unchecked';
  },
});

Template.participantesHomeFiltros.events({
  'change #checkboxInactivos': (event) => {
    event.preventDefault();
    const incluirInactivos = Session.get('incluirInactivos');
    (incluirInactivos) ? (Session.set('incluirInactivos', false)) : (Session.set('incluirInactivos', true));
  },
  'change #verColumnaFoto': (event) => {
    event.preventDefault();
    const verColumnaFoto = Session.get('verColumnaFoto');
    (verColumnaFoto) ? (Session.set('verColumnaFoto', false)) : (Session.set('verColumnaFoto', true));
  },
  'change #verColumnaEdad': (event) => {
    event.preventDefault();
    const verColumnaEdad = Session.get('verColumnaEdad');
    (verColumnaEdad) ? (Session.set('verColumnaEdad', false)) : (Session.set('verColumnaEdad', true));
  },
  'change #verColumnaEscuela': (event) => {
    event.preventDefault();
    const verColumnaEscuela = Session.get('verColumnaEscuela');
    (verColumnaEscuela) ? (Session.set('verColumnaEscuela', false)) : (Session.set('verColumnaEscuela', true));
  },
  'change #verColumnaGradoAno': (event) => {
    event.preventDefault();
    const verColumnaGradoAno = Session.get('verColumnaGradoAno');
    (verColumnaGradoAno) ? (Session.set('verColumnaGradoAno', false)) : (Session.set('verColumnaGradoAno', true));
  },
  'change #verColumnaSexo': (event) => {
    event.preventDefault();
    const verColumnaSexo = Session.get('verColumnaSexo');
    (verColumnaSexo) ? (Session.set('verColumnaSexo', false)) : (Session.set('verColumnaSexo', true));
  },
  'change #verColumnaDNI': (event) => {
    event.preventDefault();
    const verColumnaDNI = Session.get('verColumnaDNI');
    (verColumnaDNI) ? (Session.set('verColumnaDNI', false)) : (Session.set('verColumnaDNI', true));
  },
  'change #verColumnaEscolarizado': (event) => {
    event.preventDefault();
    const verColumnaEscolarizado = Session.get('verColumnaEscolarizado');
    (verColumnaEscolarizado) ? (Session.set('verColumnaEscolarizado', false)) : (Session.set('verColumnaEscolarizado', true));
  },
  'change #verColumnaEstrellas': (event) => {
    event.preventDefault();
    const verColumnaEstrellas = Session.get('verColumnaEstrellas');
    (verColumnaEstrellas) ? (Session.set('verColumnaEstrellas', false)) : (Session.set('verColumnaEstrellas', true));
  },
  'change #verColumnaDocumentos': (event) => {
    event.preventDefault();
    const verColumnaDocumentos = Session.get('verColumnaDocumentos');
    (verColumnaDocumentos) ? (Session.set('verColumnaDocumentos', false)) : (Session.set('verColumnaDocumentos', true));
  },
});
