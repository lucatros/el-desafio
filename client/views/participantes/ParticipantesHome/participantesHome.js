Template.participantesHome.onCreated(function() {
  this.autorun(() => {
    this.subscribe('participantes');
  });
});

Template.participantesHome.helpers({
  participantes() {
    return Participantes.find({}, {
      sort: {
        apellido: 1
      }
    });
  },
  docNoPresentado(documento) {
    return !documento && 'documento_no_presentado';
  },
  escolarizado(participante) {
    if ((participante.gradoAno === 'No escolarizado') || (participante.gradoAno === null)) {
      return '</span><span class="fui-cross text-danger">';
    }
    return '<span class="fui-check text-primary">';
  },
  rowInactivo(participante) {
    return (participante.activo) ? '' : 'rowInactivo';
  },
  checkboxInactivos() {
    return (Session.get('incluirInactivos')) ? 'checked' : '';
  },
  classColumnaFoto() {
    const ver = Session.get('verColumnaFoto');
    return !ver && 'hidden';
  },
  classColumnaEdad() {
    const ver = Session.get('verColumnaEdad');
    return !ver && 'hidden';
  },
  classColumnaEscuela() {
    const ver = Session.get('verColumnaEscuela');
    return !ver && 'hidden';
  },
  classColumnaGradoAno() {
    const ver = Session.get('verColumnaGradoAno');
    return !ver && 'hidden';
  },
  classColumnaSexo() {
    const ver = Session.get('verColumnaSexo');
    return !ver && 'hidden';
  },
  classColumnaDNI() {
    const ver = Session.get('verColumnaDNI');
    return !ver && 'hidden';
  },
  classColumnaEscolarizado() {
    const ver = Session.get('verColumnaEscolarizado');
    return !ver && 'hidden';
  },
  classColumnaEstrellas() {
    const ver = Session.get('verColumnaEstrellas');
    return !ver && 'hidden';
  },
  classColumnaDocumentos() {
    const ver = Session.get('verColumnaDocumentos');
    return !ver && 'hidden';
  },
});

Template.participantesHome.events({
  'mouseenter .foto_tabla': (event) => {
    $(event.currentTarget).siblings('.imagen-zoom-container').show();
  },
  'mouseleave .foto_tabla': (event) => {
    $(event.currentTarget).siblings('.imagen-zoom-container').hide();
  },
  'click .btn-filtros': (event) => {
    event.preventDefault();
    if ($('.filters-wrapper').is(':hidden')) {
      $('.filters-wrapper').slideDown('slow');
    } else {
      $('.filters-wrapper').slideUp('slow');
    }
  }
});
