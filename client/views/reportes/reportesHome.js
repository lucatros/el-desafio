Template.reportesHome.helpers({
  completado: function() {
    var talleresNoCompletados = 0;

    _.each(this.talleres, function(taller, key, list) {
      if (taller.completado != true) {
        talleresNoCompletados = talleresNoCompletados + 1;
      }
    });

    console.log(talleresNoCompletados);
    return (talleresNoCompletados === 0) ? true : false;
  }
});
