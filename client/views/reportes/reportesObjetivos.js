Template.reportesObjetivos.helpers({
  talleresConReporteCompletado: function() {
    var reporteId = Iron.controller().getParams()._id;
    var reporteObjetivos = ReportesObjetivos.findOne({
      _id: reporteId
    });
    var talleresConReporteCompletado = []; // inicializamos el array
    _.each(reporteObjetivos.talleres, function(taller, key, list) { // buscamos los talleres que ya están completados y los agregamos al array
      if (taller.completado) {
        var t = Talleres.findOne({
          _id: taller._id
        });
        talleresConReporteCompletado.push(t);
      }
    });
    return talleresConReporteCompletado;
  },
  tallerSeleccionadoId: function() {
    return Session.get("tallerSeleccionadoId");
  },

  tallerSeleccionado: function() {
    var tallerId = Session.get("tallerSeleccionadoId");
    var taller = Talleres.findOne({
      _id: tallerId
    });
    return taller;
  },
  participantesTaller: function() {
    var tallerId = Session.get("tallerSeleccionadoId");
    var participantes = ReportesObjetivosParticipantes.find({
      tallerId: tallerId
    });
    return participantes;
  },
  cortarPregunta: function(pregunta) {
    // Si la pregunta tiene más de 45 caracteres, la cortamos
    if (pregunta.length > 45) {
      return pregunta.substring(0, 42) + "...";
    } else {
      return pregunta;
    }
  },
  getApellido: function(participanteId) {
    var participante = Participantes.findOne({
      _id: participanteId
    });
    return participante.apellido;
  },
  getNombre: function(participanteId) {
    var participante = Participantes.findOne({
      _id: participanteId
    });
    return participante.nombre;
  },
  getRespuesta: function(preguntaId, participanteId) {
    var reporteId = Iron.controller().getParams()._id;
    var tallerId = Session.get("tallerSeleccionadoId");
    var respuestasParticipante = ReportesObjetivosParticipantes.findOne({
      participanteId: participanteId,
      reporteObjetivosId: reporteId,
      tallerId: tallerId
    }); // busca reportesObjetivosParticipantes del participante
    if (respuestasParticipante.respuestas) {
      var rta = _.find(respuestasParticipante.respuestas, function(respuesta, key) {
        return respuesta.preguntaId === preguntaId;
      });
      return rta.respuesta;
    } else {
      return "";
    }
  },

  graphPregunta: function() {
    var reporteId = Iron.controller().getParams()._id;
    var tallerId = Session.get("tallerSeleccionadoId");
    var preguntaId = this._id;
    var si = 0;
    var no = 0;

    var reportesPorParticipante = ReportesObjetivosParticipantes.find({
      reporteObjetivosId: reporteId,
      tallerId: tallerId
    }).fetch();
    _.each(reportesPorParticipante, function(reporte, key, list) {
      _.each(reporte.respuestas, function(rta, key, list) {
        if (rta.preguntaId === preguntaId) {
          if (rta.respuesta === "Sí") {
            si = si + 1;
          }
          if (rta.respuesta === "No") {
            no = no + 1;
          }
        }
      });
    });

    var datos = [{
      name: 'Sí',
      color: '#1abc9c',
      y: si
    }, {
      name: 'No',
      color: '#e74c3c',
      y: no
    }];

    return {
      chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false
      },
      title: {
        text: this.pregunta
      },
      tooltip: {
        pointFormat: '<b>{point.percentage:.1f}%</b>'
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          dataLabels: {
            enabled: true,
            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
            style: {
              color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
            }
          }
        }
      },
      series: [{
        type: 'pie',
        name: 'Browser share',
        data: datos
      }]
    };
  }
});

Template.reportesObjetivos.events({
  'change #selectTalleresReporteObjetivos': function(event) {
    var tallerSeleccionadoId = $(event.currentTarget).val();
    Session.set("tallerSeleccionadoId", tallerSeleccionadoId);
  }
});

Template.reportesObjetivos.nnRendered = function() {
  $('.footable').trigger('footable_redraw'); // force a redraw
};
