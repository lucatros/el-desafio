Template.reportesIndicadores.helpers({
  reportesIndicadoresIndividuales: function() {
    var reporteId = Iron.controller().getParams()._id;
    return ReportesIndicadoresParticipantes.find({
      reporteIndicadoresId: reporteId,
      completado: true
    });
  },
  nombreParticipante: function() {
    var participante = Participantes.findOne({
      _id: this.participanteId
    });
    return participante.apellido + ', ' + participante.nombre;
  },
  nombreTaller: function() {
    var taller = Talleres.findOne({
      _id: this.tallerId
    });
    return taller.nombre;
  },
  comunicacion: function() {
    var comunicacion = 0;
    comunicacion = (this.area1_1 === "Sí" ? comunicacion + 1 : comunicacion);
    comunicacion = (this.area1_1 === "A veces" ? comunicacion + 0.5 : comunicacion);
    comunicacion = (this.area1_2 === "Sí" ? comunicacion + 1 : comunicacion);
    comunicacion = (this.area1_2 === "A veces" ? comunicacion + 0.5 : comunicacion);
    comunicacion = (this.area1_3 === "Sí" ? comunicacion + 1 : comunicacion);
    comunicacion = (this.area1_3 === "A veces" ? comunicacion + 0.5 : comunicacion);
    comunicacion = (this.area1_4 === "No" ? comunicacion + 1 : comunicacion);
    comunicacion = (this.area1_4 === "A veces" ? comunicacion + 0.5 : comunicacion);
    comunicacion = (this.area1_5 === "Sí" ? comunicacion + 1 : comunicacion);
    comunicacion = (this.area1_5 === "A veces" ? comunicacion + 0.5 : comunicacion);
    comunicacion = (this.area1_6 === "Sí" ? comunicacion + 1 : comunicacion);
    comunicacion = (this.area1_6 === "A veces" ? comunicacion + 0.5 : comunicacion);
    comunicacion = (this.area1_7 === "Sí" ? comunicacion + 1 : comunicacion);
    comunicacion = (this.area1_7 === "A veces" ? comunicacion + 0.5 : comunicacion);
    comunicacion = comunicacion / 7 * 100;
    return Math.round(comunicacion);
  },
  respeto: function() {
    var respeto = 0;
    respeto = (this.area1_5 === "Sí" ? respeto + 1 : respeto);
    respeto = (this.area1_5 === "A veces" ? respeto + 0.5 : respeto);
    respeto = (this.area1_6 === "Sí" ? respeto + 1 : respeto);
    respeto = (this.area1_6 === "A veces" ? respeto + 0.5 : respeto);
    respeto = (this.area1_7 === "Sí" ? respeto + 1 : respeto);
    respeto = (this.area1_7 === "A veces" ? respeto + 0.5 : respeto);
    respeto = (this.area2_2 === "Sí" ? respeto + 1 : respeto);
    respeto = (this.area2_2 === "A veces" ? respeto + 0.5 : respeto);
    respeto = (this.area2_3 === "Sí" ? respeto + 1 : respeto);
    respeto = (this.area2_3 === "A veces" ? respeto + 0.5 : respeto);
    respeto = (this.area2_4 === "Sí" ? respeto + 1 : respeto);
    respeto = (this.area2_4 === "A veces" ? respeto + 0.5 : respeto);
    respeto = (this.area2_5 === "No" ? respeto + 1 : respeto);
    respeto = (this.area2_5 === "A veces" ? respeto + 0.5 : respeto);
    respeto = (this.area3_4 === "No" ? respeto + 1 : respeto);
    respeto = (this.area3_4 === "A veces" ? respeto + 0.5 : respeto);
    respeto = respeto / 8 * 100;
    return Math.round(respeto);
  },
  trabajo_equipo: function() {
    var trabajo_equipo = 0;
    trabajo_equipo = (this.area1_1 === "Sí" ? trabajo_equipo + 1 : trabajo_equipo);
    trabajo_equipo = (this.area1_1 === "A veces" ? trabajo_equipo + 0.5 : trabajo_equipo);
    trabajo_equipo = (this.area1_5 === "Sí" ? trabajo_equipo + 1 : trabajo_equipo);
    trabajo_equipo = (this.area1_5 === "A veces" ? trabajo_equipo + 0.5 : trabajo_equipo);
    trabajo_equipo = (this.area2_1 === "Sí" ? trabajo_equipo + 1 : trabajo_equipo);
    trabajo_equipo = (this.area2_1 === "A veces" ? trabajo_equipo + 0.5 : trabajo_equipo);
    trabajo_equipo = (this.area2_2 === "Sí" ? trabajo_equipo + 1 : trabajo_equipo);
    trabajo_equipo = (this.area2_2 === "A veces" ? trabajo_equipo + 0.5 : trabajo_equipo);
    trabajo_equipo = (this.area2_4 === "Sí" ? trabajo_equipo + 1 : trabajo_equipo);
    trabajo_equipo = (this.area2_4 === "A veces" ? trabajo_equipo + 0.5 : trabajo_equipo);
    trabajo_equipo = trabajo_equipo / 5 * 100;
    return Math.round(trabajo_equipo);

  },
  confianza: function() {
    var confianza = 0;
    confianza = (this.area1_7 === "Sí" ? confianza + 1 : confianza);
    confianza = (this.area1_7 === "A veces" ? confianza + 0.5 : confianza);
    confianza = (this.area2_1 === "Sí" ? confianza + 1 : confianza);
    confianza = (this.area2_1 === "A veces" ? confianza + 0.5 : confianza);
    confianza = (this.area3_2 === "Sí" ? confianza + 1 : confianza);
    confianza = (this.area3_2 === "A veces" ? confianza + 0.5 : confianza);
    confianza = (this.area3_6 === "No" ? confianza + 1 : confianza);
    confianza = (this.area3_6 === "A veces" ? confianza + 0.5 : confianza);
    confianza = (this.area3_7 === "Sí" ? confianza + 1 : confianza);
    confianza = (this.area3_7 === "A veces" ? confianza + 0.5 : confianza);
    confianza = (this.area3_8 === "Sí" ? confianza + 1 : confianza);
    confianza = (this.area3_8 === "A veces" ? confianza + 0.5 : confianza);
    confianza = confianza / 6 * 100;
    return Math.round(confianza);
  },
  pertenencia: function() {
    var pertenencia = 0;
    pertenencia = (this.area1_1 === "Sí" ? pertenencia + 1 : pertenencia);
    pertenencia = (this.area1_1 === "A veces" ? pertenencia + 0.5 : pertenencia);
    pertenencia = (this.area2_1 === "Sí" ? pertenencia + 1 : pertenencia);
    pertenencia = (this.area2_1 === "A veces" ? pertenencia + 0.5 : pertenencia);
    pertenencia = (this.area2_2 === "Sí" ? pertenencia + 1 : pertenencia);
    pertenencia = (this.area2_2 === "A veces" ? pertenencia + 0.5 : pertenencia);
    pertenencia = (this.area2_3 === "Sí" ? pertenencia + 1 : pertenencia);
    pertenencia = (this.area2_3 === "A veces" ? pertenencia + 0.5 : pertenencia);
    pertenencia = (this.area2_10 === "Sí" ? pertenencia + 1 : pertenencia);
    pertenencia = (this.area2_10 === "A veces" ? pertenencia + 0.5 : pertenencia);
    pertenencia = (this.area2_11 === "Sí" ? pertenencia + 1 : pertenencia);
    pertenencia = (this.area2_11 === "A veces" ? pertenencia + 0.5 : pertenencia);
    pertenencia = (this.area2_12 === "Sí" ? pertenencia + 1 : pertenencia);
    pertenencia = (this.area2_12 === "A veces" ? pertenencia + 0.5 : pertenencia);
    pertenencia = (this.area3_2 === "Sí" ? pertenencia + 1 : pertenencia);
    pertenencia = (this.area3_2 === "A veces" ? pertenencia + 0.5 : pertenencia);
    pertenencia = pertenencia / 8 * 100;
    return Math.round(pertenencia);
  },
  aduenarse: function() {
    var aduenarse = 0;
    aduenarse = (this.area2_1 === "Sí" ? aduenarse + 1 : aduenarse);
    aduenarse = (this.area2_1 === "A veces" ? aduenarse + 0.5 : aduenarse);
    aduenarse = (this.area2_2 === "Sí" ? aduenarse + 1 : aduenarse);
    aduenarse = (this.area2_2 === "A veces" ? aduenarse + 0.5 : aduenarse);
    aduenarse = (this.area2_3 === "Sí" ? aduenarse + 1 : aduenarse);
    aduenarse = (this.area2_3 === "A veces" ? aduenarse + 0.5 : aduenarse);
    aduenarse = (this.area2_6 === "Sí" ? aduenarse + 1 : aduenarse);
    aduenarse = (this.area2_6 === "A veces" ? aduenarse + 0.5 : aduenarse);
    aduenarse = (this.area2_7 === "Sí" ? aduenarse + 1 : aduenarse);
    aduenarse = (this.area2_7 === "A veces" ? aduenarse + 0.5 : aduenarse);
    aduenarse = (this.area2_10 === "Sí" ? aduenarse + 1 : aduenarse);
    aduenarse = (this.area2_10 === "A veces" ? aduenarse + 0.5 : aduenarse);
    aduenarse = (this.area2_11 === "Sí" ? aduenarse + 1 : aduenarse);
    aduenarse = (this.area2_11 === "A veces" ? aduenarse + 0.5 : aduenarse);
    aduenarse = (this.area2_12 === "Sí" ? aduenarse + 1 : aduenarse);
    aduenarse = (this.area2_12 === "A veces" ? aduenarse + 0.5 : aduenarse);
    aduenarse = aduenarse / 8 * 100;
    return Math.round(aduenarse);

  },
  compromiso: function() {
    compromiso = 0;
    compromiso = (this.area2_1 === "Sí" ? compromiso + 1 : compromiso);
    compromiso = (this.area2_1 === "A veces" ? compromiso + 0.5 : compromiso);
    compromiso = (this.area2_2 === "Sí" ? compromiso + 1 : compromiso);
    compromiso = (this.area2_2 === "A veces" ? compromiso + 0.5 : compromiso);
    compromiso = (this.area2_3 === "Sí" ? compromiso + 1 : compromiso);
    compromiso = (this.area2_3 === "A veces" ? compromiso + 0.5 : compromiso);
    compromiso = (this.area2_6 === "Sí" ? compromiso + 1 : compromiso);
    compromiso = (this.area2_6 === "A veces" ? compromiso + 0.5 : compromiso);
    compromiso = (this.area2_11 === "Sí" ? compromiso + 1 : compromiso);
    compromiso = (this.area2_11 === "A veces" ? compromiso + 0.5 : compromiso);
    compromiso = (this.area2_12 === "Sí" ? compromiso + 1 : compromiso);
    compromiso = (this.area2_12 === "A veces" ? compromiso + 0.5 : compromiso);
    compromiso = compromiso / 6 * 100;
    return Math.round(compromiso);
  },
  autoestima: function() {
    var autoestima = 0;
    autoestima = (this.area1_1 === "Sí" ? autoestima + 1 : autoestima);
    autoestima = (this.area1_1 === "A veces" ? autoestima + 0.5 : autoestima);
    autoestima = (this.area1_5 === "Sí" ? autoestima + 1 : autoestima);
    autoestima = (this.area1_5 === "A veces" ? autoestima + 0.5 : autoestima);
    autoestima = (this.area2_8 === "Sí" ? autoestima + 1 : autoestima);
    autoestima = (this.area2_8 === "A veces" ? autoestima + 0.5 : autoestima);
    autoestima = (this.area2_9 === "Sí" ? autoestima + 1 : autoestima);
    autoestima = (this.area2_9 === "A veces" ? autoestima + 0.5 : autoestima);
    autoestima = (this.area3_1 === "Sí" ? autoestima + 1 : autoestima);
    autoestima = (this.area3_1 === "A veces" ? autoestima + 0.5 : autoestima);
    autoestima = (this.area3_2 === "Sí" ? autoestima + 1 : autoestima);
    autoestima = (this.area3_2 === "A veces" ? autoestima + 0.5 : autoestima);
    autoestima = (this.area3_6 === "No" ? autoestima + 1 : autoestima);
    autoestima = (this.area3_6 === "A veces" ? autoestima + 0.5 : autoestima);
    autoestima = (this.area3_9 === "Sí" ? autoestima + 1 : autoestima);
    autoestima = (this.area3_9 === "A veces" ? autoestima + 0.5 : autoestima);
    autoestima = autoestima / 8 * 100;
    return Math.round(autoestima);
  },
  frustracion: function() {
    var frustracion = 0;
    frustracion = (this.area2_1 === "No" ? frustracion + 1 : frustracion);
    frustracion = (this.area2_1 === "A veces" ? frustracion + 0.5 : frustracion);
    frustracion = (this.area2_2 === "No" ? frustracion + 1 : frustracion);
    frustracion = (this.area2_2 === "A veces" ? frustracion + 0.5 : frustracion);
    frustracion = (this.area2_3 === "No" ? frustracion + 1 : frustracion);
    frustracion = (this.area2_3 === "A veces" ? frustracion + 0.5 : frustracion);
    frustracion = (this.area2_5 === "Sí" ? frustracion + 1 : frustracion);
    frustracion = (this.area2_5 === "A veces" ? frustracion + 0.5 : frustracion);
    frustracion = (this.area3_4 === "Sí" ? frustracion + 1 : frustracion);
    frustracion = (this.area3_4 === "A veces" ? frustracion + 0.5 : frustracion);
    frustracion = (this.area3_5 === "Sí" ? frustracion + 1 : frustracion);
    frustracion = (this.area3_5 === "A veces" ? frustracion + 0.5 : frustracion);
    frustracion = (this.area3_6 === "Sí" ? frustracion + 1 : frustracion);
    frustracion = (this.area3_6 === "A veces" ? frustracion + 0.5 : frustracion);
    frustracion = (this.area3_7 === "No" ? frustracion + 1 : frustracion);
    frustracion = (this.area3_7 === "A veces" ? frustracion + 0.5 : frustracion);
    frustracion = frustracion / 8 * 100;
    return Math.round(frustracion);
  },
  conflicto: function() {
    var conflicto = 0;
    conflicto = (this.area1_1 === "No" ? conflicto + 1 : conflicto);
    conflicto = (this.area1_1 === "A veces" ? conflicto + 0.5 : conflicto);
    conflicto = (this.area1_4 === "Sí" ? conflicto + 1 : conflicto);
    conflicto = (this.area1_4 === "A veces" ? conflicto + 0.5 : conflicto);
    conflicto = (this.area2_1 === "No" ? conflicto + 1 : conflicto);
    conflicto = (this.area2_1 === "A veces" ? conflicto + 0.5 : conflicto);
    conflicto = (this.area2_2 === "No" ? conflicto + 1 : conflicto);
    conflicto = (this.area2_2 === "A veces" ? conflicto + 0.5 : conflicto);
    conflicto = (this.area2_3 === "No" ? conflicto + 1 : conflicto);
    conflicto = (this.area2_3 === "A veces" ? conflicto + 0.5 : conflicto);
    conflicto = (this.area3_4 === "Sí" ? conflicto + 1 : conflicto);
    conflicto = (this.area3_4 === "A veces" ? conflicto + 0.5 : conflicto);
    conflicto = (this.area3_5 === "Sí" ? conflicto + 1 : conflicto);
    conflicto = (this.area3_5 === "A veces" ? conflicto + 0.5 : conflicto);
    conflicto = conflicto / 7 * 100;
    return Math.round(conflicto);
  },
  identidad: function() {
    var identidad = 0;
    identidad = (this.area1_5 === "Sí" ? identidad + 1 : identidad);
    identidad = (this.area1_5 === "A veces" ? identidad + 0.5 : identidad);
    identidad = (this.area2_7 === "Sí" ? identidad + 1 : identidad);
    identidad = (this.area2_7 === "A veces" ? identidad + 0.5 : identidad);
    identidad = (this.area2_8 === "Sí" ? identidad + 1 : identidad);
    identidad = (this.area2_8 === "A veces" ? identidad + 0.5 : identidad);
    identidad = (this.area2_9 === "Sí" ? identidad + 1 : identidad);
    identidad = (this.area2_9 === "A veces" ? identidad + 0.5 : identidad);
    identidad = (this.area3_1 === "Sí" ? identidad + 1 : identidad);
    identidad = (this.area3_1 === "A veces" ? identidad + 0.5 : identidad);
    identidad = (this.area3_2 === "Sí" ? identidad + 1 : identidad);
    identidad = (this.area3_2 === "A veces" ? identidad + 0.5 : identidad);
    identidad = (this.area3_3 === "No" ? identidad + 1 : identidad);
    identidad = (this.area3_3 === "A veces" ? identidad + 0.5 : identidad);
    identidad = (this.area3_6 === "No" ? identidad + 1 : identidad);
    identidad = (this.area3_6 === "A veces" ? identidad + 0.5 : identidad);
    identidad = identidad / 8 * 100;
    return Math.round(identidad);
  },
  liderazgo: function() {
    var liderazgo = 0;
    liderazgo = (this.area2_7 === "Sí" ? liderazgo + 1 : liderazgo);
    liderazgo = (this.area2_7 === "A veces" ? liderazgo + 0.5 : liderazgo);
    liderazgo = (this.area3_1 === "Sí" ? liderazgo + 1 : liderazgo);
    liderazgo = (this.area3_1 === "A veces" ? liderazgo + 0.5 : liderazgo);
    liderazgo = (this.area3_3 === "No" ? liderazgo + 1 : liderazgo);
    liderazgo = (this.area3_3 === "A veces" ? liderazgo + 0.5 : liderazgo);
    liderazgo = liderazgo / 3 * 100;
    return Math.round(liderazgo);
  },
  superacion: function() {
    var superacion = 0;
    superacion = (this.area3_6 === "No" ? superacion + 1 : superacion);
    superacion = (this.area3_6 === "A veces" ? superacion + 0.5 : superacion);
    superacion = (this.area3_8 === "Sí" ? superacion + 1 : superacion);
    superacion = (this.area3_8 === "A veces" ? superacion + 0.5 : superacion);
    superacion = (this.area3_9 === "Sí" ? superacion + 1 : superacion);
    superacion = (this.area3_9 === "A veces" ? superacion + 0.5 : superacion);
    superacion = superacion / 3 * 100;
    return Math.round(superacion);
  }
});

Template.reportesIndicadores.rendered = function() {
  function calcular_totales_reporte_indicadores() {
    var cantidad_filas_visibles = $('.fila_parcial:visible').length;

    var total_comunicacion = 0;
    $('.parcial_comunicacion:visible').each(function(index, value) {
      total_comunicacion = total_comunicacion + parseInt($(this).text(), 10);
    });
    total_comunicacion = total_comunicacion / cantidad_filas_visibles;
    $('#total_comunicacion').html(total_comunicacion.toFixed());

    var total_respeto = 0;
    $('.parcial_respeto:visible').each(function(index, value) {
      total_respeto = total_respeto + parseInt($(this).text(), 10);
    });
    total_respeto = total_respeto / cantidad_filas_visibles;
    $('#total_respeto').html(total_respeto.toFixed());


    var total_trabajo_equipo = 0;
    $('.parcial_trabajo_equipo:visible').each(function(index, value) {
      total_trabajo_equipo = total_trabajo_equipo + parseInt($(this).text(), 10);
    });
    total_trabajo_equipo = total_trabajo_equipo / cantidad_filas_visibles;
    $('#total_trabajo_equipo').html(total_trabajo_equipo.toFixed());


    var total_confianza = 0;
    $('.parcial_confianza:visible').each(function(index, value) {
      total_confianza = total_confianza + parseInt($(this).text(), 10);
    });
    total_confianza = total_confianza / cantidad_filas_visibles;
    $('#total_confianza').html(total_confianza.toFixed());


    var total_pertenencia = 0;
    $('.parcial_pertenencia:visible').each(function(index, value) {
      total_pertenencia = total_pertenencia + parseInt($(this).text(), 10);
    });
    total_pertenencia = total_pertenencia / cantidad_filas_visibles;
    $('#total_pertenencia').html(total_pertenencia.toFixed());


    var total_aduenarse = 0;
    $('.parcial_aduenarse:visible').each(function(index, value) {
      total_aduenarse = total_aduenarse + parseInt($(this).text(), 10);
    });
    total_aduenarse = total_aduenarse / cantidad_filas_visibles;
    $('#total_aduenarse').html(total_aduenarse.toFixed());


    var total_compromiso = 0;
    $('.parcial_compromiso:visible').each(function(index, value) {
      total_compromiso = total_compromiso + parseInt($(this).text(), 10);
    });
    total_compromiso = total_compromiso / cantidad_filas_visibles;
    $('#total_compromiso').html(total_compromiso.toFixed());


    var total_autoestima = 0;
    $('.parcial_autoestima:visible').each(function(index, value) {
      total_autoestima = total_autoestima + parseInt($(this).text(), 10);
    });
    total_autoestima = total_autoestima / cantidad_filas_visibles;
    $('#total_autoestima').html(total_autoestima.toFixed());


    var total_frustracion = 0;
    $('.parcial_frustracion:visible').each(function(index, value) {
      total_frustracion = total_frustracion + parseInt($(this).text(), 10);
    });
    total_frustracion = total_frustracion / cantidad_filas_visibles;
    $('#total_frustracion').html(total_frustracion.toFixed());


    var total_conflicto = 0;
    $('.parcial_conflicto:visible').each(function(index, value) {
      total_conflicto = total_conflicto + parseInt($(this).text(), 10);
    });
    total_conflicto = total_conflicto / cantidad_filas_visibles;
    $('#total_conflicto').html(total_conflicto.toFixed());


    var total_identidad = 0;
    $('.parcial_identidad:visible').each(function(index, value) {
      total_identidad = total_identidad + parseInt($(this).text(), 10);
    });
    total_identidad = total_identidad / cantidad_filas_visibles;
    $('#total_identidad').html(total_identidad.toFixed());


    var total_liderazgo = 0;
    $('.parcial_liderazgo:visible').each(function(index, value) {
      total_liderazgo = total_liderazgo + parseInt($(this).text(), 10);
    });
    total_liderazgo = total_liderazgo / cantidad_filas_visibles;
    $('#total_liderazgo').html(total_liderazgo.toFixed());


    var total_superacion = 0;
    $('.parcial_superacion:visible').each(function(index, value) {
      total_superacion = total_superacion + parseInt($(this).text(), 10);
    });
    total_superacion = total_superacion / cantidad_filas_visibles;
    $('#total_superacion').html(total_superacion.toFixed());
  } // Fin función

  calcular_totales_reporte_indicadores();

  $('table').footable().bind({
    'footable_filtered': function(e) {
      $('.fila_totales').removeAttr("style");
      calcular_totales_reporte_indicadores();
    },
  });
};
