// Function for converting DataURL to Blob
function dataURLtoBlob(dataurl) {
  var arr = dataurl.split(','),
    mime = arr[0].match(/:(.*?);/)[1],
    bstr = atob(arr[1]),
    n = bstr.length,
    u8arr = new Uint8Array(n);
  while (n--) {
    u8arr[n] = bstr.charCodeAt(n);
  }
  return new Blob([u8arr], {
    type: mime
  });
}

var uploader = new ReactiveVar();

Template.fotoPerfilForm.onCreated( () => {
  // We create a reactive dict var for the pageSession (temporary vars for the page)
  // We avoid using Sessions so we don't polute the Global scope
  Template.instance().pageSession = new ReactiveDict();
  Session.set('isUploading', false);
});


Template.fotoPerfilForm.onRendered( () => {
  let photoUrl = Template.instance().data.foto;
  if (!photoUrl) {
    Template.instance().pageSession.set('photoUrl', '/images/profile_picture.png');
  } else {
    Template.instance().pageSession.set('photoUrl', photoUrl);
  }
});


Template.fotoPerfilForm.helpers({
  photoUrl() {
    return Template.instance().pageSession.get('photoUrl');
  },
  progress() {
    let upload = uploader.get();
    console.log(upload);
    if (upload) {
      return Math.round(upload.progress() * 100);
    }
  },
  isUploading() {
    const isUploading = Boolean(uploader.get());
    (isUploading) ? Session.set('isUploading', true) : Session.set('isUploading', false);

    return Boolean(uploader.get());
  }
});

Template.fotoPerfilForm.events({
  'click .js-cambiar-foto': (event, template) => {
    const cameraOptions = {
      width: 350,
      heigth: 350
    };
    MeteorCamera.getPicture([cameraOptions], function(error, newPhoto) {
      template.pageSession.set('photoUrl', newPhoto);

      // Upload the image to S3
      let upload = new Slingshot.Upload("photoUpload");
      upload.send(dataURLtoBlob(newPhoto), function(error2, downloadUrl) {
        uploader.set();
        if (error2) {
          // Log service detailed response.
          console.error('Error uploading', uploader.xhr.response);
          alert (error2);
        }
        else {

          // Assign the URL to the foto input element
          document.getElementById("fotoUrl").value = downloadUrl;

        }
        
      });
      uploader.set(upload);
    });
  }
});
