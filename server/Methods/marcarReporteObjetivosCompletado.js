Meteor.methods({
	marcarReporteObjetivosCompletado: function (tallerId, reporteObjetivosId) {
		ReportesObjetivos.update({_id: reporteObjetivosId, talleres: { $elemMatch: { _id: tallerId }}}, {$set: { "talleres.$.completado": true } });
	}
});
