Meteor.methods({
	cantidadReportesIndicadoresACompletar: function (tallerId) {
		var x = ReportesIndicadores.find({talleres: { $elemMatch: { _id: tallerId, completado: false } }}).count();
		return x; 
	}
});

Meteor.methods({
	cantidadReportesObjetivosACompletar: function (tallerId) {
		var x = ReportesObjetivos.find({talleres: { $elemMatch: { _id: tallerId, completado: false } }}).count();
		return x; 
	}
});



