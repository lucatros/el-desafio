Meteor.methods({
	marcarReporteIndicadoresCompletado: function (tallerId, reporteIndicadoresId) {
		ReportesIndicadores.update({_id: reporteIndicadoresId, talleres: { $elemMatch: { _id: tallerId }}}, {$set: { "talleres.$.completado": true } });
	}
});
