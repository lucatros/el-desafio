Meteor.methods({
  habilitarReporte: function(doc) {
    check(doc, Schema.ReporteHabilitar);

    if (doc.tipo === "Por indicadores") {
      // Si es "Por indicadores", creamos uno nuevo, con la fecha del doc
      var reporteId = ReportesIndicadores.insert({
        fecha: doc.fecha
      }, {
        validate: false
      });

      // Vemos cual es el año del reporte a crear así agregamos los talleres de ese año al reporte
      var anoReporte = moment.utc(doc.fecha).year();

      // Buscamos los talleres del anoReporte
      var talleresReporte = Talleres.find({
        ano: anoReporte
      }).fetch();

      // Agregamos cada taller al reporte y los participantes activos del mismo
      _.each(talleresReporte, function(taller, key, list) {
        var tallerAdd = {
          _id: taller._id,
          completado: false
        };
        // agregamos el taller al reporte
        ReportesIndicadores.update({
          _id: reporteId
        }, {
          $addToSet: {
            talleres: tallerAdd
          }
        }, {
          validate: false
        });

        // Agregamos a cada participante activo al reporte.
        _.each(taller.participantesActivos, function(participanteId, key2, list2) {
          ReportesIndicadoresParticipantes.insert({
            reporteIndicadoresId: reporteId,
            tallerId: taller._id,
            participanteId: participanteId
          }, {
            validate: false
          });
        });
      });
    }
    if (doc.tipo === "Por objetivos") {
      // Si es "Por indicadores", creamos uno nuevo, con la fecha del doc
      var reporteId = ReportesObjetivos.insert({
        fecha: doc.fecha
      }, {
        validate: false
      });

      // Vemos cual es el año del reporte a crear así agregamos los talleres de ese año al reporte
      var anoReporte = moment.utc(doc.fecha).year();

      // Buscamos los talleres del anoReporte
      var talleresReporte = Talleres.find({
        ano: anoReporte
      }).fetch();

      // Agregamos cada taller al reporte y los participantes activos del mismo
      _.each(talleresReporte, function(taller, key, list) {
        var tallerAdd = {
          _id: taller._id,
          completado: false
        };
        // agregamos el taller al reporte
        ReportesObjetivos.update({
          _id: reporteId
        }, {
          $addToSet: {
            talleres: tallerAdd
          }
        }, {
          validate: false
        });

        // Agregamos a cada participante activo al reporte.
        _.each(taller.participantesActivos, function(participanteId, key2, list2) {
          ReportesObjetivosParticipantes.insert({
            reporteObjetivosId: reporteId,
            tallerId: taller._id,
            participanteId: participanteId
          }, {
            validate: false
          });
        });
      });
    }
  }
});
