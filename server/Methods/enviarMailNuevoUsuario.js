Meteor.methods({
  enviarMailNuevoUsuario: function(formUserId) {
    Meteor.setTimeout(function() {
      Accounts.sendEnrollmentEmail(formUserId);
    }, 2 * 1000);
  }
});
