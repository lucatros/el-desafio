Meteor.methods({
	marcarParticipanteInactivo: function (participanteId) {
		var rta = Talleres.update(
								{ participantesActivos: { $in: [participanteId] } }, 
								{
									$pull: {participantesActivos: participanteId},  
									$addToSet: {participantesInactivos: participanteId}}
								);
		console.log(rta);
		return rta;
	}
});
