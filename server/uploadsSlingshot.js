Slingshot.createDirective("photoUpload", Slingshot.S3Storage, {
  bucket: "photosprofile",

  acl: "public-read",

  authorize: function() {
    // Deny uploads if user is not logged in.
    if (!this.userId) {
      var message = "Please login before posting files";
      throw new Meteor.Error("Login Required", message);
    }

    return true;
  },

  key: function(file) {
    // Store file
    var ext = file.type.split(/\//).pop();
    var dir = 'profilepics/';
    var fn = Meteor.uuid() + "." + ext;
    return dir + fn;
  }
});
