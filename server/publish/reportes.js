// Todos los talleres
Meteor.publish('reportesIndicadores', function() {
  return [
    ReportesIndicadores.find()
  ];
});

Meteor.publish('reporteIndicadores', function(id) {
  return [
    ReportesIndicadores.find({
      _id: id
    })
  ];
});


Meteor.publish('reportesIndicadoresTaller', function(tallerId) {
  return [
    ReportesIndicadores.find({
      talleres: {
        $elemMatch: {
          _id: tallerId
        }
      }
    })
  ];
});

Meteor.publish('reportesIndicadoresParticipantesTaller', function(tallerId, reporteindicadoresid) {
  return [
    ReportesIndicadoresParticipantes.find({
      reporteIndicadoresId: reporteindicadoresid,
      tallerId: tallerId
    })
  ];
});


Meteor.publish('reportesIndicadoresParticipantes', function(id) {
  return [
    ReportesIndicadoresParticipantes.find({
      _id: id
    })
  ];
});

Meteor.publish('reportesIndicadoresParticipantesDelReporte', function(reporteId) {
  return [
    ReportesIndicadoresParticipantes.find({
      reporteIndicadoresId: reporteId
    })
  ];
});

/*=================================
=            Objetivos            =
=================================*/

Meteor.publish('reportesObjetivos', function() {
  return [
    ReportesObjetivos.find()
  ];
});

Meteor.publish('reporteObjetivos', function(id) {
  return [
    ReportesObjetivos.find({
      _id: id
    })
  ];
});


Meteor.publish('reportesObjetivosTaller', function(tallerId) {
  return [
    ReportesObjetivos.find({
      talleres: {
        $elemMatch: {
          _id: tallerId
        }
      }
    })
  ];
});

Meteor.publish('reportesObjetivosParticipantesTaller', function(tallerId, reporteobjetivosid) {
  return [
    ReportesObjetivosParticipantes.find({
      reporteObjetivosId: reporteobjetivosid,
      tallerId: tallerId
    })
  ];
});

Meteor.publish('reportesObjetivosParticipantes', function(id) {
  return [
    ReportesObjetivosParticipantes.find({
      _id: id
    })
  ];
});

Meteor.publish('reportesObjetivosTodosParticipantes', function(reporteObjetivosId) {
  return [
    ReportesObjetivosParticipantes.find({
      reporteObjetivosId: reporteObjetivosId
    })
  ];
});
