// Todos los talleres
Meteor.publish('talleres', function() {
  return [
    Talleres.find()
  ];
});

// Taller individual
Meteor.publish('taller', function(id) {
  return Talleres.find({
    _id: id
  });
});


// Participantes de taller. Se usa para particpantesActivos, Inactivos, etc.
// Se espera un array
Meteor.publish('participantesTaller', function(participantesArray) {
  if (participantesArray) {
    return Participantes.find({
      _id: {
        $in: participantesArray
      }
    });
  }
  this.ready(); // Importante!!! ver https://github.com/meteor/meteor/issues/1633
});

Meteor.publish('fotoParticipantesTaller', function(participantesArray) {
  if (participantesArray) {
    var images = [];
    _.each(participantesArray, function(participanteId, key, list) {
      p = Participantes.findOne(participanteId);
      images.push(p.foto);
    });
    return Images.find({
      _id: {
        $in: images
      }
    });
  }
  this.ready(); // Importante!!! ver https://github.com/meteor/meteor/issues/1633
});
