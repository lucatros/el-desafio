// Participantes de taller. Se usa para particpantesActivos, Inactivos, etc.
// Se espera un array
Meteor.publish('clasesTaller', function(tallerId) {
  if (tallerId) {
    return Clases.find({
      tallerId: tallerId
    });
  }
  this.ready(); // Importante!!! ver https://github.com/meteor/meteor/issues/1633
});

Meteor.publish('clase', function(claseId) {
  if (claseId) {
    return Clases.find({
      _id: claseId
    });
  }
  this.ready(); // Importante!!! ver https://github.com/meteor/meteor/issues/1633
});

Meteor.publish('clases', function() {
  return Clases.find();
  this.ready(); // Importante!!! ver https://github.com/meteor/meteor/issues/1633
});


Meteor.publish('clasesParticipante', function(participanteId) {
  if (participanteId) {
    return Clases.find({
      participantes: {
        $elemMatch: {
          id: participanteId
        }
      }
    });
  }
  this.ready(); // Importante!!! ver https://github.com/meteor/meteor/issues/1633
});
