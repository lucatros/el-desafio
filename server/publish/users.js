Meteor.publish('users', function() {
  return [
    Users.find(),
    Images.find()
  ];
});

// USer individual
Meteor.publish('user', function(id) {
  return Users.find({
    _id: id
  });
});

// Devuelve los talleristas y voluntarios del taller
Meteor.publish('talleristasYvoluntariosTaller', function(talleristasOvoluntariosArray) {
  if (talleristasOvoluntariosArray) {
    return Users.find({
      _id: {
        $in: talleristasOvoluntariosArray
      }
    });
  }
  this.ready(); // Importante!!! ver https://github.com/meteor/meteor/issues/1633
});
