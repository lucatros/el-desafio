// Todos los participantes
Meteor.publish('participantes', function(incluirInactivos) {
  if (incluirInactivos) {
    return Participantes.find({}, {
      fields: {
        datosMedicos: 0,
        diario: 0
      }
    });
  }

  return Participantes.find({ activo: true}, {
    fields: {
      datosMedicos: 0,
      diario: 0
    }
  });
});







// NOTE: Below this line all the code might be old and might not be used
// anywhere.

// Todos los participantes - SOlo nombre, apellido y _id
Meteor.publish('participantesNombreApellido', function() {
  return [
    Participantes.find({}, {
      fields: {
        nombre: 1,
        apellido: 1
      }
    })
  ];
});


// Participante individual
Meteor.publish('participante', function(id) {
  return Participantes.find({
    _id: id
  });
});

// Foto del participante
Meteor.publish('foto', function(id) {
  return Images.find({
    _id: id
  });
});

// Talleres del participante
Meteor.publish('participanteTalleres', function(id) {
  return Talleres.find(
    // Solo devuelve los talleres en los que el participante está activo, inactivo o en espera
    // y solamente devuielve los campos pertinentes
    {
      $or: [{
        participantesActivos: {
          $in: [id]
        }
      }, {
        participantesInactivos: {
          $in: [id]
        }
      }, {
        participantesEspera: {
          $in: [id]
        }
      }]
    }, {
      fields: {
        nombre: 1,
        participantesActivos: 1,
        participantesInactivos: 1,
        participantesEspera: 1
      }
    }
  );
});

// Todos los familiares
Meteor.publish('familiares', function() {
  return Familiares.find();
});

Meteor.publish('familiar', function(id) {
  return Familiares.find({
    _id: id
  });
});