Images.files.permit(['insert', 'update', 'remove']).apply();

Images.allow({
  download: function(userId, doc) {
    return true;
  }
});

// Familiares.permit(['insert', 'update', 'remove']).apply();

// Participantes.permit(['insert', 'update', 'remove']).apply();

// Talleres.permit(['insert', 'update', 'remove']).apply();

// Users.permit(['insert', 'update', 'remove']).apply();

Security.permit(['insert', 'update', 'remove']).collections([
  Familiares,
  Participantes,
  Talleres,
  Users,
  Clases,
  ReportesIndicadoresParticipantes,
  ReportesIndicadores,
  ReportesObjetivosParticipantes,
  ReportesObjetivos
]).apply();


//  Any client may insert, update, or remove a post without restriction
// Posts.permit(['insert', 'update', 'remove']).apply();

//  No clients may insert, update, or remove posts
// Posts.permit(['insert', 'update', 'remove']).never().apply();

//  Clients may insert posts only if a user is logged in
// Posts.permit('insert').ifLoggedIn().apply();

//  Clients may remove posts only if an admin user is logged in
// Posts.permit('remove').ifHasRole('admin').apply();

//  Admin users may update any properties of any post, but regular users may
//  update posts only if they don't try to change the `author` or `date` properties
// Posts.permit('update').ifHasRole('admin').apply();
// Posts.permit('update').ifLoggedIn().exceptProps(['author', 'date']).apply();
