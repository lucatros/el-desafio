Meteor.startup(function() {
  process.env.MAIL_URL = Meteor.settings.MAIL_URL;

  // Mails que se envían al usuario
  // By default, the email is sent from no-reply@meteor.com. If you wish to receive email from users asking for help with their account, be sure to set this to an email address that you can receive email at.
  Accounts.emailTemplates.from = 'El Desafio <no-reply@eldesafio.org>';

  // The public name of your application. Defaults to the DNS name of the application (eg: awesome.meteor.com).
  Accounts.emailTemplates.siteName = 'El Desafio - Sistema';

  // A Function that takes a user object and returns a String for the subject line of the email.
  Accounts.emailTemplates.enrollAccount.subject = function(user) {
    return 'Se creo una cuenta para vos en El Desafio (sistema)';
  };

  // A Function that takes a user object and a url, and returns the body text for the email.
  // Note: if you need to return HTML instead, use Accounts.emailTemplates.verifyEmail.html
  Accounts.emailTemplates.enrollAccount.text = function(user, url) {
    return 'Hacé click en el siguiente link para verificar tu dirección de correo electrónico y crear tu contraseña: ' + url;
  };

  // Si se reinicia la DB, se va a crear este usuario.
  var cantidadUsuarios = Meteor.users.find().count();
  if (cantidadUsuarios === 0) {
    var userId = Accounts.createUser({
      email: 'lucas.curti@eldesafio.org',
      profile: {
        nombre: 'Lucas',
        apellido: 'Curti'
      },
      roles: ['admin']
    });
  }
});
