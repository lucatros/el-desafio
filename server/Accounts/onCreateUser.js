Accounts.onCreateUser(function(options, user) {
  if (options.rolesHelper) {
    user.rolesHelper = options.rolesHelper;
  }
  if (options.roles) {
    user.roles = options.roles;
  }
  // We still want the default hook's 'profile' behavior.
  if (options.profile) {
    user.profile = options.profile;
  }

  Meteor.setTimeout(function() {
    Accounts.sendEnrollmentEmail(user._id);
  }, 2 * 3000);

  return user;
});
